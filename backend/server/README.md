#Namestitev strežnika

```git clone https://github.com/klemenzarn/IoTPark/```

database dump se nahaja v /database_dump/iotpark.sql

Potrebno imeti nameščeno nodejs in npm

1. ```npm install nodemon -g```
2. ```npm install pm2 -g```
3. ```cd /backend/server```
4. ```npm install```
5. konfiguracija .env datoteke (database settings in port) <- če bo database na localhost ni potrebno spreminjati, trenutni port je nastavljen na 3000
6. dodatna konfiguracija s podatkovno bazo se spreminja v fajlu: ```/app/config/database.js``` (database port, username, password, db name)
7. start server:

  7.1 Development mode: ```npm run dev```
  
  7.2 Production mode: ```npm run prodStart``` (kasneje bolj priporočljivo: ```npm run prodStartMaxCPU```, da se zažene server na vseh jedrih, pri ```npm run prodStart``` se samo na enem)
8. stop server:

  8.1 Develoment mode: crtl + c v terminalu
  
  8.1 Production mode: ```npm run prodStop```
  
Ko prvič strežnik uspešno deluje in ko je pm2 instaliran je potrebno linkati z Keymetrics (monitoring strežnika (CPU, RAM, ...), ob izpadu javi na mail, ...) računom:

```pm2 link db99p2bxwevimh4 k91xeym7sud3u8p IoTParkServer```
