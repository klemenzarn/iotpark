var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var Error = require('./app/core/errors/Error');
var flash = require('express-flash-messages')
var session = require('express-session');

var apiRouterV1 = require('./app/api/v1/api');
var index = require('./admin/routes/index');
var parking = require('./admin/routes/parking');
var provider = require('./admin/routes/provider')

//init express server
var app = express();

app.set('views', path.join(__dirname, 'admin/views'));
app.set('view engine', 'ejs');


app.use(session({
    cookie: { maxAge: 60000 },
    secret: 'sessionSecretIotPark',
    resave: false,
    saveUninitialized: false
}));
app.use(expressLayouts);
app.use(flash())

app.set('json spaces', 4);
app.use(helmet({
    noCache: true
}));

//init body parser for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());


//init endpoints
app.use(express.static(path.join(__dirname, 'admin/public')));
app.use('/api', apiRouterV1); //init RESTful API on /api endpoint
app.use('/', index);
app.use('/parking', parking);
app.use('/provider', provider);

//error handling
app.use((err, req, res, next) => {
    err = new Error(err);

    Log.error('error happened when requesting ' + req.url, {
        url: req.url,
        error: err
    });

    res.status(err.status).json(err.outputData());
})


module.exports = app;