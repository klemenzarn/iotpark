function createLookup(currentArray) {
    var outputLookup = [];

    currentArray.forEach(function(element) {
        outputLookup[element.day] = element;
    });

    return outputLookup;
}

function initDailySelectionInputs() {
    var template_workingHours = $('#template_workingHours').html();
    var template_parkingFee = $('#template_parkingFee').html();
    Mustache.parse(template_workingHours);
    Mustache.parse(template_parkingFee);

    var days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    parkingWorkingHours = JSON.parse(parkingWorkingHours);
    var currentWorkingHoursLookup = createLookup(parkingWorkingHours);

    parkingFeesByDay = JSON.parse(parkingFeesByDay);
    var currentFeeByDayLookup = createLookup(parkingFeesByDay);

    var renderedWorkingHours = '';
    var renderedParkingFee = '';

    var selectedDay = false;
    var allDay = false;
    var start = '';
    var dayFee = '';
    var end = '';

    days.forEach(function(day, index) {

        selectedDay = false;
        allDay = false;
        start = '';
        end = '';

        var currentDayWorkingHours = currentWorkingHoursLookup[index];
        if (currentDayWorkingHours != undefined) {
            selectedDay = true;
            allDay = currentDayWorkingHours.allDay;
            if (!allDay) {
                start = currentDayWorkingHours.start;
                end = currentDayWorkingHours.end;
            }
        }

        renderedWorkingHours += Mustache.render(template_workingHours, {
            selected: selectedDay,
            day: day,
            dayIndex: index,
            allDay: allDay,
            start: start,
            end: end
        });

        selectedDay = false;
        dayFee = '';
        start = '';
        end = '';

        var currentDayFeeData = currentFeeByDayLookup[index];
        if (currentDayFeeData != undefined) {
            selectedDay = true;
            dayFee = currentDayFeeData.fee;
            start = currentDayFeeData.start;
            end = currentDayFeeData.end;
        }

        renderedParkingFee += Mustache.render(template_parkingFee, {
            selected: selectedDay,
            day: day,
            dayIndex: index,
            fee: dayFee,
            start: start,
            end: end
        });
    });

    $('#workingHoursSelection').html(renderedWorkingHours);
    $('#parkingFeeSelection').html(renderedParkingFee);
}