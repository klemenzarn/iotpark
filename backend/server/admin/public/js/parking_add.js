$(document).ready(function() {
    initDailySelectionInputs();

    initICheckBox();

    initTimePicker();

    initGeoCodingAddress();
});

function initGeoCodingAddress() {
    $('.parkingAddressInput').change(function() {
        var addressCombined = '';
        $('.parkingAddressInput').each(function() {
            addressCombined += $(this).val() + ' ';
        });

        geocodeAddress(addressCombined);
    });
}

function initTimePicker() {
    $(".timepicker").timepicker({
        showInputs: false,
        showMeridian: false
    });

}


/*
//for testing insert only
(function() {
    $('#feeVaryByDay').iCheck('check');
    $('#Tuesday_parking').iCheck('check');
    $('#Wednesday_hours').iCheck('check');
    $('#Wednesday_always_open').iCheck('check');
    $('#Sunday_hours').iCheck('check');
    $('select[name="provider"] option[value="1"]').prop('selected', true);
    $('select[name="manager"] option[value="1"]').prop('selected', true);
    $('select[name="type"] option[value="1"]').prop('selected', true);
    $('select[name="category"] option[value="1"]').prop('selected', true);
});
*/