function initICheckBox() {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
    });

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-',
        radioClass: 'iradio_flat-'
    });

    var checkFeeFreeSelection = function() {
        if ($('#freeFee')[0].checked) {
            $('#feeVaryByDay').iCheck('disable');
        } else {
            $('#feeVaryByDay').iCheck('enable');
        }

        setDisabledStatusForFeeInput();
    };
    $('#freeFee').on('ifChanged', checkFeeFreeSelection);
    checkFeeFreeSelection();

    var checkFeeVarySelection = function() {
        if ($('#feeVaryByDay')[0].checked) {
            $('#parkingFeeSelection').show();
            $('#freeFee').iCheck('disable');
        } else {
            $('#parkingFeeSelection').hide();
            $('#freeFee').iCheck('enable');
        }

        setDisabledStatusForFeeInput();
    };
    $('#feeVaryByDay').on('ifChanged', checkFeeVarySelection);
    checkFeeVarySelection();

    var onDaySelectionCheckboxChanged = function() {
        var name = $(this).attr('name').trim();
        var inputDisabled = !this.checked;

        $('input[name="' + name + '_start"]').prop('disabled', inputDisabled);
        $('input[name="' + name + '_end"]').prop('disabled', inputDisabled);
        $('input[name="' + name + '_fee"]').prop('disabled', inputDisabled);
        $('input[name="' + name + '_alwaysOpen"]').iCheck(inputDisabled ? 'disable' : 'enable');
    };
    $('.daySelection').on('ifChanged', onDaySelectionCheckboxChanged);
    $('.daySelection').each(onDaySelectionCheckboxChanged);

    var onAlwaysOpenCheckboxChanged = function() {
        var name = $(this).attr('data-parent-name');
        var daySelectionChecked = $('input[name="' + name + '"]')[0].checked;
        var inputDisabled = this.checked;

        if (daySelectionChecked) {
            $('input[name="' + name + '_start"]').prop('disabled', inputDisabled);
            $('input[name="' + name + '_end"]').prop('disabled', inputDisabled);
        }
    };
    $('.alwayOpenSelection').on('ifChecked', onAlwaysOpenCheckboxChanged);
    $('.alwayOpenSelection').on('ifUnchecked', onAlwaysOpenCheckboxChanged);
    $('.alwayOpenSelection').each(onAlwaysOpenCheckboxChanged);

    $('input[type=radio][name=openParkingType]').on('ifChecked', function() {
        if (this.value == '1') {
            $('#workingHoursSelection').show();
        } else {
            $('#workingHoursSelection').hide();
        }
    });

    var selectedOpenParkingType = $('input[type=radio][name=openParkingType]:checked').val();
    if (selectedOpenParkingType == '1') {
        $('#workingHoursSelection').show();
    } else {
        $('#workingHoursSelection').hide();
    }
}

function setDisabledStatusForFeeInput() {
    $('#inputFeePerHour').prop('disabled', $('#feeVaryByDay')[0].checked || $('#freeFee')[0].checked);
}