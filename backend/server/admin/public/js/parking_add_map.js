var map;
var geocoder;
var mapMarker;

google.maps.event.addDomListener(window, 'load', initializeMap);


function initializeMap() {
    geocoder = new google.maps.Geocoder();

    var initialLocation = {
        lat: parseFloat($('#parkingLat').val()),
        lng: parseFloat($('#parkingLng').val())
    };

    var mapOptions = {
        zoom: 12,
        center: initialLocation
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    mapMarker = new google.maps.Marker({
        position: initialLocation,
        map: map,
        draggable: true
    });

    google.maps.event.addListener(mapMarker, 'dragend', function() {
        var position = mapMarker.getPosition();
        setLatLng(position);
    });
}

function geocodePosition(latLng) {
    geocoder.geocode({
        latLng: latLng
    }, function(responses) {
        if (responses && responses.length > 0) {
            console.log(responses);
        } else {
            //TODO DIALOG
            console.log('Neznan naslov.');
        }
    });
}

function geocodeAddress(address) {
    geocoder.geocode({ 
        address: address
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latLng = results[0].geometry.location;
            mapMarker.setPosition(latLng);
            map.setCenter(latLng);
            setLatLng(latLng);
        } else {
            //TODO dialog
            console.log("Unknown location");
        }
    });
}

function setLatLng(position) {
    var lat = position.lat();
    var lng = position.lng();
    $('#parkingLat').val(lat);
    $('#parkingLng').val(lng);
}

