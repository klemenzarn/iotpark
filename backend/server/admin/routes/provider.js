var express = require('express');
var router = express.Router();

var ForbiddenError = require('../../app/core/errors/ForbiddenError');
var NotFoundError = require('../../app/core/errors/NotFoundError');
var Error = require('../../app/core/errors/Error');
var Log = require('../../app/core/Log');
var AuthCheck = require('../../app/core/AuthCheck');

var Models = require('../../app/model/models');

router.use(AuthCheck);

router.get('/', asyncWrap(async(req, res, next) => {
    res.end('TODO LIST PROVIDERS');
}));

router.get('/add', asyncWrap(async(req, res, next) => {
    res.end('TODO CREATE PROVIDER FORM');
}));

module.exports = router;