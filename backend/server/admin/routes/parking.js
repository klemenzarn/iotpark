var express = require('express');
var router = express.Router();

var ForbiddenError = require('../../app/core/errors/ForbiddenError');
var NotFoundError = require('../../app/core/errors/NotFoundError');
var Error = require('../../app/core/errors/Error');
var Log = require('../../app/core/Log');
var AuthCheck = require('../../app/core/AuthCheck');

var Models = require('../../app/model/models');
var Parking = Models.Parking;
var Area = Models.Area;
var Slot = Models.Slot;
var AreaOccupancy = Models.AreaOccupancy;
var ParkingOccupancy = Models.ParkingOccupancy;
var ParkingType = Models.ParkingType;
var ParkingCategory = Models.ParkingCategory;
var Provider = Models.Provider;
var Manager = Models.Manager;
var WorkingType = Models.WorkingType;

let defaultNonValue = 'unknown';



router.use(AuthCheck);

router.get('/', asyncWrap(async(req, res, next) => {
    let overallParkingsResults = await Parking.overallAll();

    let outputParkingData = overallParkingsResults;

    outputParkingData.forEach(function(parking) {

        parking.capacity = defaultNonValue;
        parking.occupancy = defaultNonValue;
        parking.free = defaultNonValue;
        parking.occupancyPercent = defaultNonValue;

        if (parking.parking_occupancy) {
            parking.capacity = parking.parking_occupancy.capacity;
            if (parking.parking_occupancy.occupancy != null) {
                parking.occupancy = parking.parking_occupancy.occupancy;
                parking.free = parking.capacity - parking.occupancy;
                parking.occupancyPercent = parseInt(Math.round((parking.occupancy / parking.capacity) * 100.0)) + '%';
            }
        }

        let category;
        if (parking.parking_category != null) {
            category = parking.parking_category.category;
        }

        let type;
        if (parking.parking_type != null) {
            if (category == undefined) {
                type = parking.parking_type.type;
            } else {
                type = '(' + parking.parking_type.type + ')';
            }
        }

        let googleMapsQuery = parking.name;

        let parkingType = defaultNonValue;
        if (category != undefined || type != undefined) {
            parkingType = category + ' ' + type
        }

        let parkingAddress = defaultNonValue;
        if (parking.address != null) {
            parkingAddress = parking.address.addressName + ', ' + parking.address.city;
            googleMapsQuery = parkingAddress;
        }

        let parkingProvider = defaultNonValue;
        if (parking.providers != null && parking.providers.length > 0) {
            parkingProvider = parking.providers[0].name;
        }

        let status = defaultNonValue;
        if (parking.working_type != null) {
            status = parking.working_type.type;
        }

        if (parking.location != null) {
            googleMapsQuery = parking.location.lat + ', ' + parking.location.lng;
            if (parkingAddress == defaultNonValue) {
                parkingAddress = googleMapsQuery;
            }
        }

        let parkingPayment = defaultNonValue;
        if (parking.fee_type != null) {
            if (parking.fee_type.id == 2) {
                parkingPayment = parking.fee + ' €/h';
            } else {
                parkingPayment = parking.fee_type.type;
            }
        }

        parking.googleMapsQuery = googleMapsQuery;
        parking.type = parkingType;
        parking.addressData = parkingAddress;
        parking.provider = parkingProvider;
        parking.status = status;
        parking.paymentStatus = parkingPayment;

    });

    res.render('admin/parking/parking_list', {
        parkings: outputParkingData
    });
}));

//TODO delete providers, image size, photocropper, login 

router.get('/edit/:id', asyncWrap(async(req, res, next) => {
    let id = req.params.id;
    let parking = await Parking.overallOne({ id: id });

    if (parking != null) {

        var promiseArray = [];

        promiseArray.push(ParkingType.findAll());
        promiseArray.push(ParkingCategory.findAll());
        promiseArray.push(Manager.findAll());
        promiseArray.push(Provider.findAll());
        promiseArray.push(WorkingType.findAll());

        var results = await Promise.all(promiseArray);

        parking = fixParkingDataForView(parking);

        res.render('admin/parking/parking_add', {
            types: results[0],
            categories: results[1],
            managers: results[2],
            providers: results[3],
            workingTypes: results[4],
            edit: true,
            parking: parking,
            selectedProvider: parking.selectedProvider,
            selectedManager: parking.selectedManager,
            formAction: '/api/parking/edit',
        });
    } else {
        next(new NotFoundError());
    }
}));

router.get('/add', asyncWrap(async(req, res, next) => {

    var promiseArray = [];

    promiseArray.push(ParkingType.findAll());
    promiseArray.push(ParkingCategory.findAll());
    promiseArray.push(Manager.findAll());
    promiseArray.push(Provider.findAll());
    promiseArray.push(WorkingType.findAll());

    var results = await Promise.all(promiseArray);

    let parking = fixParkingDataForView({});

    res.render('admin/parking/parking_add', {
        types: results[0],
        categories: results[1],
        managers: results[2],
        providers: results[3],
        workingTypes: results[4],
        edit: false,
        parking: parking,
        selectedProvider: parking.selectedProvider,
        selectedManager: parking.selectedManager,
        formAction: '/api/parking/add',
    });
}));


function fixParkingDataForView(parking) {
    let propsNeededFix = [
        'address',
        { name: 'parking_category', data: { id: -1 } },
        { name: 'parking_type', data: { id: -1 } },
        'parking_occupancy',
        'fee_type',
        'working_type',
        { name: 'location', data: { lat: 46.553719, lng: 15.6536 } },
        { name: 'providers', data: [{ id: -1 }] },
        { name: 'managers', data: [{ id: -1 }] },
        { name: 'working_hours', data: [] },
        { name: 'parking_fee_by_days', data: [] }
    ];

    propsNeededFix.forEach(prop => {
        let propName = prop.name ? prop.name : prop;
        if (!parking[propName]) {
            parking[propName] = prop.data ? prop.data : {};
        }
    });

    if(parking.published == undefined){
        parking.published = false;
    }

    let selectedProvider = -1;
    if (parking.providers.length > 0 && parking.providers[0].id != undefined) {
        selectedProvider = parking.providers[0].id;
    }

    let selectedManager = -1;
    if (parking.managers.length > 0 && parking.managers[0].id != undefined) {
        selectedManager = parking.managers[0].id;
    }

    parking.selectedManager = selectedManager;
    parking.selectedProvider = selectedProvider;

    return parking;
}

module.exports = router;