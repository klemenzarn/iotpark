var express = require('express');
var router = express.Router();
var AuthCheck = require('../../app/core/AuthCheck');

var Log = require('../../app/core/Log');
var Request = require('../../app/core/Request');

router.use(AuthCheck);

router.get('/', (req, res, next) => {
    res.redirect('/parking');
});



module.exports = router;