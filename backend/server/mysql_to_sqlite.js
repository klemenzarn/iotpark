var Sequelize = require('sequelize');
var fs = require('fs');
global.Sequelize = Sequelize;

var mysqlConfig = {
    username: 'root',
    password: '',
    database: 'IoTPark_new',
    connection: {
        host: 'localhost',
        dialect: 'mysql',
        logging: false,
        pool: {
            max: 100,
            min: 0,
            idle: 10000
        },
    }
};

var sqliteConfig = {
    username: '',
    password: '',
    database: '',
    connection: {
        host: 'localhost',
        dialect: 'sqlite',
        logging: false,
        pool: {
            max: 1000,
            min: 0,
            idle: 10000
        },


        storage: 'mysql_to_sqlite.sqlite'
    }
};


let isCopy = false;

if (isCopy) {
    const sequelizeMySQL = new Sequelize(mysqlConfig.database, mysqlConfig.username, mysqlConfig.password, mysqlConfig.connection);
    global.sequelize = sequelizeMySQL;
    var models = require('./app/model/models');


    (async() => {

        await global.sequelize.sync();

        let Parking = await models.Parking.findAll();
        let Area = await models.Area.findAll();
        let Address = await models.Address.findAll();
        let Slot = await models.Slot.findAll();
        let ParkingOccupancy = await models.ParkingOccupancy.findAll();
        let AreaOccupancy = await models.AreaOccupancy.findAll();
        let ParkingType = await models.ParkingType.findAll();
        let ParkingCategory = await models.ParkingCategory.findAll();
        let Provider = await models.Provider.findAll();
        let Manager = await models.Manager.findAll();
        let WorkingType = await models.WorkingType.findAll();
        let WorkingHour = await models.WorkingHour.findAll();
        let FeeByDays = await models.FeeByDays.findAll();
        let FeeType = await models.FeeType.findAll();
        let Location = await models.Location.findAll();

        let ParkingProvider = await sequelize.query("SELECT * FROM `parking_provider`", { type: sequelize.QueryTypes.SELECT });
        let ParkingManager = await sequelize.query("SELECT * FROM `parking_manager`", { type: sequelize.QueryTypes.SELECT });

        var json = JSON.stringify({
            Parking,
            Area,
            Address,
            Slot,
            ParkingOccupancy,
            AreaOccupancy,
            ParkingType,
            ParkingCategory,
            Provider,
            Manager,
            WorkingType,
            WorkingHour,
            FeeByDays,
            FeeType,
            Location,
            ParkingProvider,
            ParkingManager
        });

        fs.writeFile('mysql_to_sqlite.json', json, 'utf8', error => {
            if (error)
                console.log('error happened', error);
            else
                console.log('data dumped');
        });
    })();

} else {
    const sequelizeSQLite = new Sequelize(sqliteConfig.database, sqliteConfig.username, sqliteConfig.password, sqliteConfig.connection);
    global.sequelize = sequelizeSQLite;
    var models = require('./app/model/models');

    let json = fs.readFileSync('mysql_to_sqlite.json', 'utf-8');
    let data = JSON.parse(json);


    (async() => {

        await global.sequelize.sync({ force: true });

        try {
            await models.FeeType.bulkCreate(data.FeeType);
            await models.WorkingType.bulkCreate(data.WorkingType);
            await models.ParkingType.bulkCreate(data.ParkingType);
            await models.ParkingCategory.bulkCreate(data.ParkingCategory);
            await models.Manager.bulkCreate(data.Manager);
                        await models.Address.bulkCreate(data.Address);


            await models.Provider.bulkCreate(data.Provider);
            await models.Parking.bulkCreate(data.Parking);
            await models.Area.bulkCreate(data.Area);
            await models.Slot.bulkCreate(data.Slot);
            await models.ParkingOccupancy.bulkCreate(data.ParkingOccupancy);
            await models.AreaOccupancy.bulkCreate(data.AreaOccupancy);
            await models.FeeByDays.bulkCreate(data.FeeByDays);
            await models.WorkingHour.bulkCreate(data.WorkingHour);
            await models.Location.bulkCreate(data.Location);

            data.ParkingProvider.forEach(async providerData => {
                try {
                    await sequelize.query("INSERT INTO `parking_provider` (providerId, parkingId, createdAt, updatedAt) VALUES (?, ?, ?, ?)", { replacements: [providerData.providerId, providerData.parkingId, new Date(), new Date()] });
                } catch (error1) {
                    console.log('ParkingProvider err: ', error1)
                }
            });

            data.ParkingManager.forEach(async managerData => {
                try {
                    await sequelize.query("INSERT INTO `parking_manager` (managerId, parkingId, createdAt, updatedAt) VALUES (?, ?, ?, ?)", { replacements: [managerData.managerId, managerData.parkingId, new Date(), new Date()] });
                } catch (error1) {
                    console.log('ParkingManager err: ', error1)
                }
            });
        } catch (err) {
            console.log(err);
        };


    })();

    //todo
}