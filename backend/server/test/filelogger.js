var charAsPromised = require('chai-as-promised');
var chai = require('chai').use(charAsPromised);
var should = chai.should();
var expect = chai.expect;

var fs = require('fs');
var Log = require('../app/core/Log');
var FileLogger = require('../app/core/logger/FileLogger');

var loggingFile = 'test/test_log.log';

var getLogFileSize = function(loggingFile) {
    if (fs.existsSync(loggingFile)) {
        const stats = fs.statSync(loggingFile);
        return stats.size;
    }

    return 0;
}

var checkIfIsFileChanged = function(before, loggingFile, done) {
    setTimeout(function() {
        var after = getLogFileSize(loggingFile);
        expect(before).to.be.lessThan(after);
        done();
    }, 100);
}

describe("FileLogger", function() {

    describe("logging", function() {


        it("it logs info", function(done) {
            let fileLog = 'test/testLogs/info_test_log.log';

            Log.init(new FileLogger(fileLog));
            var before = getLogFileSize(fileLog);

            Log.info('test info', { data: 'test info' });

            checkIfIsFileChanged(before, fileLog, done);
        });

        it("it logs warn", function(done) {
            let fileLog = 'test/testLogs/warn_test_log.log';

            Log.init(new FileLogger(fileLog));
            var before = getLogFileSize(fileLog);

            Log.warn('test warn', { data: 'test warn' });


            checkIfIsFileChanged(before, fileLog, done);
        });

        it("it logs error", function(done) {
            let fileLog = 'test/testLogs/error_test_log.log';

            Log.init(new FileLogger(fileLog));
            var before = getLogFileSize(fileLog);

            Log.error('test error', { data: 'test error' });

            checkIfIsFileChanged(before, fileLog, done);
        });


    });
});