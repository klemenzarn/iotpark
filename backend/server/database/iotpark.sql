-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Gostitelj: 127.0.0.1
-- Čas nastanka: 22. jun 2017 ob 13.20
-- Različica strežnika: 10.1.21-MariaDB
-- Različica PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Zbirka podatkov: `iotpark_new`
--

-- --------------------------------------------------------

--
-- Struktura tabele `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `addressName` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `addresses`
--

INSERT INTO `addresses` (`id`, `addressName`, `zip`, `city`, `createdAt`, `updatedAt`) VALUES
(78, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Plecnikova 15', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Tržaška cesta 23', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Tržaška cesta 23', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'Tržaška cesta 233', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'Bohorska cesta 4', '8281', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'Bohorska cesta 4 5', '82814', 'Senovo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'Pobreška cesta 18', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'Pobreška cesta 18', '2000', 'Maribor', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabele `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `shortName` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `fullName` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `parkingId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `areas`
--

INSERT INTO `areas` (`id`, `shortName`, `fullName`, `hash`, `createdAt`, `updatedAt`, `parkingId`) VALUES
(1, 'P1', 'P1 Zeleno/Oranžne Barve', '01db4104bc0431e80d149732440651be', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(3, 'P2', 'P2 Rdeče Barve', 'd475304c6e9293d1f5e3a9619afe61e9', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(4, 'P3', 'P3 Rumene Barve', '1416aad55ed12098a9022ae975b5d938', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(5, 'P4', 'P4 Modre Barve', 'e1e5003451577aad20823e3aec2daffc', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(6, 'E1', 'Etaža 1', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
(7, 'E2', 'Etaža 2', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
(8, 'K', 'Klet', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
(9, 'P5', 'P5 Bele Barve', '3d1c225eba6589e7c54eb549d6c9e6e2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struktura tabele `area_occupancies`
--

CREATE TABLE `area_occupancies` (
  `id` int(11) NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  `occupancy` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `areaId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `area_occupancies`
--

INSERT INTO `area_occupancies` (`id`, `capacity`, `occupancy`, `createdAt`, `updatedAt`, `areaId`) VALUES
(472, 961, 782, '0000-00-00 00:00:00', '2017-06-04 07:46:48', 1),
(473, 291, 115, '0000-00-00 00:00:00', '2017-06-04 07:46:48', 3),
(474, 338, 336, '0000-00-00 00:00:00', '2017-06-04 07:46:48', 4),
(475, 450, 11, '0000-00-00 00:00:00', '2017-06-04 08:04:11', 9),
(476, 339, 316, '0000-00-00 00:00:00', '2017-06-04 08:04:11', 5);

-- --------------------------------------------------------

--
-- Struktura tabele `fee_types`
--

CREATE TABLE `fee_types` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `fee_types`
--

INSERT INTO `fee_types` (`id`, `type`) VALUES
(1, 'Free'),
(2, 'Payable'),
(3, 'Vary by day/hour');

-- --------------------------------------------------------

--
-- Struktura tabele `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `polyline` text COLLATE utf8mb4_slovenian_ci,
  `parkingId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `locations`
--

INSERT INTO `locations` (`id`, `lat`, `lng`, `polyline`, `parkingId`) VALUES
(1, 46.553719, 15.6536, NULL, NULL),
(2, 46.552253, 15.647278, NULL, 2),
(3, 45, 17, NULL, NULL),
(4, 45, 17, NULL, NULL),
(5, 45, 17, NULL, NULL),
(6, 45, 17, NULL, NULL),
(7, 45, 17, NULL, NULL),
(8, 46.55371900000001, 15.654973291015608, NULL, NULL),
(9, 46.55371900000001, 15.654973291015608, NULL, NULL),
(10, 46.55371900000001, 15.654973291015608, NULL, NULL),
(11, 46.55371900000001, 15.654973291015608, NULL, NULL),
(12, 46.55371900000001, 15.654973291015608, NULL, NULL),
(13, 46.55371900000001, 15.654973291015608, NULL, NULL),
(14, 46.55371900000001, 15.654973291015608, NULL, NULL),
(15, 46.55371900000001, 15.654973291015608, NULL, NULL),
(16, 45, 17, NULL, NULL),
(17, 45, 17, NULL, NULL),
(18, 45, 17, NULL, NULL),
(19, 45, 17, NULL, NULL),
(20, 45, 17, NULL, NULL),
(21, 45, 17, NULL, NULL),
(22, 45, 17, NULL, NULL),
(23, 45, 17, NULL, NULL),
(24, 45, 17, NULL, NULL),
(25, 45, 17, NULL, NULL),
(26, 45, 17, NULL, NULL),
(27, 45, 17, NULL, NULL),
(28, 45, 17, NULL, NULL),
(29, 45, 17, NULL, NULL),
(30, 45, 17, NULL, NULL),
(31, 45, 17, NULL, NULL),
(32, 45, 17, NULL, NULL),
(33, 45, 17, NULL, NULL),
(34, 45, 17, NULL, NULL),
(35, 45, 17, NULL, NULL),
(36, 45, 17, NULL, NULL),
(37, 45, 17, NULL, NULL),
(38, 45, 17, NULL, NULL),
(39, 45, 17, NULL, NULL),
(40, 45, 17, NULL, NULL),
(41, 45, 17, NULL, NULL),
(42, 45, 17, NULL, NULL),
(43, 45, 17, NULL, NULL),
(44, 45, 17, NULL, NULL),
(45, 45, 17, NULL, NULL),
(46, 45, 17, NULL, NULL),
(47, 45, 17, NULL, NULL),
(48, 45, 17, NULL, NULL),
(49, 46.0200172, 15.475312499999973, NULL, NULL),
(50, 46.0200172, 15.475312499999973, NULL, NULL),
(51, 46.0200172, 15.475312499999973, NULL, NULL),
(52, 46.0200172, 15.475312499999973, NULL, NULL),
(53, 46.0200172, 15.475312499999973, NULL, NULL),
(54, 46.0200172, 15.475312499999973, NULL, NULL),
(55, 46.0200172, 15.475312499999973, NULL, NULL),
(56, 46.0200172, 15.475312499999973, NULL, NULL),
(57, 46.5476115, 15.647721100000012, NULL, NULL),
(58, 46.5476115, 15.647721100000012, NULL, NULL),
(59, 46.5476115, 15.647721100000012, NULL, NULL),
(60, 46.5476115, 15.647721100000012, NULL, NULL),
(61, 46.5476115, 15.647721100000012, NULL, NULL),
(62, 46.5476115, 15.647721100000012, NULL, NULL),
(63, 46.5476115, 15.647721100000012, NULL, NULL),
(64, 46.5476115, 15.647721100000012, NULL, NULL),
(65, 46.0200172, 15.475312499999973, NULL, NULL),
(66, 46.0200172, 15.475312499999973, NULL, 71),
(67, 46.0200172, 15.475312499999973, NULL, NULL),
(68, 46.0200172, 15.475312499999973, NULL, NULL),
(69, 46.5346142, 15.647993600000063, NULL, NULL),
(70, 46.5346142, 15.647993600000063, NULL, NULL),
(71, 46.0200172, 15.475312499999973, NULL, NULL),
(72, 46.5346142, 15.647993600000063, NULL, NULL),
(73, 46.5346142, 15.647993600000063, NULL, NULL),
(74, 46.5346142, 15.647993600000063, NULL, NULL),
(75, 46.5346142, 15.647993600000063, NULL, NULL),
(76, 46.0200172, 15.475312499999973, NULL, 60),
(77, 46.5346142, 15.647993600000063, NULL, NULL),
(78, 46.5346142, 15.647993600000063, NULL, NULL),
(79, 46.5346142, 15.647993600000063, NULL, NULL),
(80, 46.0200172, 15.475312499999973, NULL, NULL),
(81, 46.0200172, 15.475312499999973, NULL, NULL),
(82, 46.0200172, 15.475312499999973, NULL, NULL),
(83, 46.0200172, 15.475312499999973, NULL, NULL),
(84, 46.0200172, 15.475312499999973, NULL, NULL),
(85, 46.0200172, 15.475312499999973, NULL, NULL),
(86, 46.5346142, 15.647993600000063, NULL, NULL),
(87, 46.5346142, 15.647993600000063, NULL, 75),
(88, 46.0200172, 15.475312499999973, NULL, NULL),
(89, 45.995694468150894, 15.399781494140598, NULL, 77),
(90, 46.5539774, 15.653144099999963, NULL, NULL),
(91, 46.5539774, 15.653144099999963, NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabele `managers`
--

CREATE TABLE `managers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `managers`
--

INSERT INTO `managers` (`id`, `name`) VALUES
(1, 'Versor');

-- --------------------------------------------------------

--
-- Struktura tabele `parkings`
--

CREATE TABLE `parkings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `hasSlotData` tinyint(1) DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `parkingTypeId` int(11) DEFAULT NULL,
  `parkingCategoryId` int(11) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `workingTypeId` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `webSite` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `feeTypeId` int(11) DEFAULT NULL,
  `fee` double NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `addressId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `parkings`
--

INSERT INTO `parkings` (`id`, `name`, `hash`, `hasSlotData`, `createdAt`, `updatedAt`, `parkingTypeId`, `parkingCategoryId`, `published`, `workingTypeId`, `phone`, `webSite`, `feeTypeId`, `fee`, `image`, `addressId`) VALUES
(1, 'Europark', '0baa3ddb9bb1ba47bf42987d0a2e70c0', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 2, 1, 1, '', '', 1, 0, 'Europark-1498128356221.jpeg', 118),
(2, 'UKC', '0baa3ddb9bb1ba47bf42987d0a2e733d', 0, '2017-06-03 19:32:53', '2017-06-03 19:32:53', NULL, NULL, 1, 2, NULL, NULL, 3, 0, NULL, NULL),
(60, 'Mlinska ulica', 'fc7c8e221fa6dc47aaf59cf802e1db25a0f1d6d6', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, 1, 1, '+38670878752', 'iotpark.com', 1, 0, 'Klemen-Zarn-1497438225383.jpeg', 102),
(71, 'Lent', '96ddd9d6114dfcef860f94e9fa9745b5b3514b48', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 1, 1, 1, '+38670878752', 'iotpark.com', 1, 0, 'Testno-parkrišče-1497438621656.jpeg', 92),
(75, 'Rakušev trg', '2bc94f701d9446e8351faa19cb3b7b3e20d34da5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, 1, 2, '070878752', '', 1, 0, 'Versor-1-1497880890146.jpeg', 114),
(77, 'Sodna ulica', '7dc782d289b3e1e90df6c1fac6d036323ea2e37d', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 1, 1, 1, '+38670878752', '', 3, 0, 'Klemen-Zarn 3 5-1498129990679.jpeg', 116);

-- --------------------------------------------------------

--
-- Struktura tabele `parking_categories`
--

CREATE TABLE `parking_categories` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `parking_categories`
--

INSERT INTO `parking_categories` (`id`, `category`, `createdAt`, `updatedAt`) VALUES
(1, 'Indoor', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Outdoor', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabele `parking_fee_by_days`
--

CREATE TABLE `parking_fee_by_days` (
  `id` int(11) NOT NULL,
  `day` int(11) DEFAULT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `fee` double DEFAULT NULL,
  `parkingId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `parking_fee_by_days`
--

INSERT INTO `parking_fee_by_days` (`id`, `day`, `start`, `end`, `fee`, `parkingId`) VALUES
(1, 1, '08:45:00', '08:45:00', NULL, NULL),
(2, 1, '08:45:00', '08:45:00', 0, NULL),
(3, 1, '09:30:00', '09:30:00', 0, NULL),
(4, 1, '10:15:00', '10:15:00', 0, NULL),
(5, 1, '10:15:00', '10:15:00', 0, NULL),
(6, 1, '10:15:00', '10:15:00', 0, NULL),
(7, 1, '10:15:00', '10:15:00', 0, NULL),
(8, 1, '10:15:00', '10:15:00', 0, NULL),
(9, 1, '10:15:00', '10:15:00', 0, NULL),
(10, 0, '08:00:00', '08:00:00', 0, NULL),
(11, 1, '08:00:00', '08:00:00', 0, NULL),
(12, 3, '09:00:00', '13:30:00', 0, NULL),
(13, 0, '08:00:00', '08:00:00', 0, NULL),
(14, 1, '08:00:00', '08:00:00', 0, NULL),
(15, 3, '09:00:00', '13:30:00', 0, NULL),
(16, 0, '08:00:00', '08:00:00', 0, NULL),
(17, 1, '08:00:00', '08:00:00', 0, NULL),
(18, 3, '09:00:00', '13:30:00', 0, NULL),
(19, 0, '08:00:00', '08:00:00', 0, NULL),
(20, 1, '08:00:00', '08:00:00', 0, NULL),
(21, 3, '09:00:00', '13:30:00', 0, NULL),
(22, 0, '08:00:00', '08:00:00', 0, NULL),
(23, 1, '08:00:00', '08:00:00', 0, NULL),
(24, 3, '09:00:00', '13:30:00', 0, NULL),
(25, 0, '09:30:00', '09:30:00', 1, NULL),
(26, 1, '09:30:00', '09:30:00', 2, NULL),
(30, 0, '13:00:00', '13:00:00', 4, 77),
(31, 1, '13:00:00', '13:00:00', 5, 77);

-- --------------------------------------------------------

--
-- Struktura tabele `parking_manager`
--

CREATE TABLE `parking_manager` (
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `managerId` int(11) NOT NULL,
  `parkingId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `parking_manager`
--

INSERT INTO `parking_manager` (`createdAt`, `updatedAt`, `managerId`, `parkingId`) VALUES
('2017-06-22 11:16:26', '2017-06-22 11:16:26', 1, 1),
('2017-06-22 10:05:58', '2017-06-22 10:05:58', 1, 60),
('2017-06-19 07:50:00', '2017-06-19 07:50:00', 1, 71),
('2017-06-22 11:19:20', '2017-06-22 11:19:20', 1, 75),
('2017-06-22 11:13:10', '2017-06-22 11:13:10', 1, 77);

-- --------------------------------------------------------

--
-- Struktura tabele `parking_occupancies`
--

CREATE TABLE `parking_occupancies` (
  `id` int(11) NOT NULL,
  `capacity` int(11) DEFAULT NULL,
  `occupancy` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `parkingId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `parking_occupancies`
--

INSERT INTO `parking_occupancies` (`id`, `capacity`, `occupancy`, `createdAt`, `updatedAt`, `parkingId`) VALUES
(4, 2393, 1560, '2017-06-04 07:40:45', '2017-06-04 08:03:56', NULL),
(5, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(6, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 2384, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 2384, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, 2384, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(10, 2384, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(11, 2384, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(12, 2384, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(13, 2384, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 2384, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 0, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 2041, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(18, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(19, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(20, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(21, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(22, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(23, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(24, 5555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(25, 2041, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(26, 2041, 1951, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 71),
(27, 555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(28, 555, 526, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(29, 5002, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(30, 5002, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(31, 5002, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(32, 5002, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(33, 5002, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(34, 200, 181, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(35, 100, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 60),
(36, 200, 181, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(37, 200, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(38, 200, 181, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(39, 2555, 2526, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(40, 2555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(41, 2555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(42, 2555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(43, 2555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(44, 200, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(45, 2005, 1986, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 75),
(46, 2555, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(47, 2555, 2526, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 77),
(48, 2393, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(49, 2393, 1560, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struktura tabele `parking_provider`
--

CREATE TABLE `parking_provider` (
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `providerId` int(11) NOT NULL,
  `parkingId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `parking_provider`
--

INSERT INTO `parking_provider` (`createdAt`, `updatedAt`, `providerId`, `parkingId`) VALUES
('2017-06-22 11:16:26', '2017-06-22 11:16:26', 1, 1),
('2017-06-22 10:05:58', '2017-06-22 10:05:58', 1, 60),
('2017-06-19 07:50:00', '2017-06-19 07:50:00', 1, 71),
('2017-06-22 11:19:20', '2017-06-22 11:19:20', 2, 75),
('2017-06-22 11:13:10', '2017-06-22 11:13:10', 2, 77);

-- --------------------------------------------------------

--
-- Struktura tabele `parking_types`
--

CREATE TABLE `parking_types` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `parking_types`
--

INSERT INTO `parking_types` (`id`, `type`, `createdAt`, `updatedAt`) VALUES
(1, 'Private', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Public', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabele `providers`
--

CREATE TABLE `providers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `providers`
--

INSERT INTO `providers` (`id`, `name`) VALUES
(1, 'Versor'),
(2, 'Traffic design');

-- --------------------------------------------------------

--
-- Struktura tabele `slots`
--

CREATE TABLE `slots` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL,
  `occupied` tinyint(1) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `areaId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

-- --------------------------------------------------------

--
-- Struktura tabele `working_hours`
--

CREATE TABLE `working_hours` (
  `id` int(11) NOT NULL,
  `day` int(11) DEFAULT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `allDay` tinyint(1) NOT NULL DEFAULT '0',
  `parkingId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `working_hours`
--

INSERT INTO `working_hours` (`id`, `day`, `start`, `end`, `allDay`, `parkingId`) VALUES
(1, 0, '05:18:24', '06:00:00', 0, NULL),
(2, NULL, NULL, NULL, 1, NULL),
(3, NULL, NULL, NULL, 1, NULL),
(4, 2, NULL, NULL, 1, NULL),
(5, 2, NULL, NULL, 1, NULL),
(6, 6, '03:15:00', '03:15:00', 1, NULL),
(7, 2, NULL, NULL, 1, NULL),
(8, 6, '15:30:00', '15:30:00', 1, NULL),
(9, 2, NULL, NULL, 1, NULL),
(10, 6, '15:30:00', '15:30:00', 1, NULL),
(11, 2, NULL, NULL, 1, NULL),
(12, 6, '15:30:00', '15:30:00', 1, NULL),
(13, 2, NULL, NULL, 1, NULL),
(14, 6, '15:30:00', '19:30:00', 1, NULL),
(15, 2, NULL, NULL, 1, NULL),
(16, 6, '08:45:00', '08:45:00', 1, NULL),
(17, 2, NULL, NULL, 1, NULL),
(18, 6, '08:45:00', '08:45:00', 0, NULL),
(19, 2, NULL, NULL, 1, NULL),
(20, 6, '08:45:00', '08:45:00', 0, NULL),
(21, 2, NULL, NULL, 1, NULL),
(22, 6, '09:00:00', '09:00:00', 0, NULL),
(23, 2, NULL, NULL, 1, NULL),
(24, 6, '09:00:00', '09:00:00', 0, NULL),
(25, 2, NULL, NULL, 1, NULL),
(26, 6, '09:00:00', '09:00:00', 0, NULL),
(27, 2, NULL, NULL, 1, NULL),
(28, 6, '09:00:00', '09:00:00', 0, NULL),
(29, 2, NULL, NULL, 1, NULL),
(30, 6, '09:00:00', '09:00:00', 0, NULL),
(31, 2, NULL, NULL, 1, NULL),
(32, 6, '09:00:00', '09:00:00', 0, NULL),
(33, 2, NULL, NULL, 1, NULL),
(34, 6, '09:00:00', '09:00:00', 0, NULL),
(35, 2, NULL, NULL, 1, NULL),
(36, 6, '09:00:00', '09:00:00', 0, NULL),
(37, 2, NULL, NULL, 1, NULL),
(38, 6, '09:00:00', '09:00:00', 0, NULL),
(39, 2, NULL, NULL, 1, NULL),
(40, 6, '09:00:00', '09:00:00', 0, NULL),
(41, 2, NULL, NULL, 1, NULL),
(42, 6, '09:00:00', '09:00:00', 0, NULL),
(43, 2, NULL, NULL, 1, NULL),
(44, 6, '09:00:00', '09:00:00', 0, NULL),
(45, 2, NULL, NULL, 1, NULL),
(46, 6, '09:00:00', '09:00:00', 0, NULL),
(47, 2, NULL, NULL, 1, NULL),
(48, 6, '09:00:00', '09:00:00', 0, NULL),
(49, 2, NULL, NULL, 1, NULL),
(50, 6, '09:00:00', '09:00:00', 0, NULL),
(51, 2, NULL, NULL, 1, NULL),
(52, 6, '09:00:00', '09:00:00', 0, NULL),
(53, 2, NULL, NULL, 1, NULL),
(54, 6, '09:00:00', '09:00:00', 0, NULL),
(55, 2, NULL, NULL, 1, NULL),
(56, 6, '09:00:00', '09:00:00', 0, NULL),
(57, 2, NULL, NULL, 1, NULL),
(58, 6, '09:30:00', '09:30:00', 0, NULL),
(59, 2, NULL, NULL, 1, NULL),
(60, 6, '10:15:00', '10:15:00', 0, NULL),
(61, 2, NULL, NULL, 1, NULL),
(62, 6, '10:15:00', '10:15:00', 0, NULL),
(63, 2, NULL, NULL, 1, NULL),
(64, 6, '10:15:00', '10:15:00', 0, NULL),
(65, 2, NULL, NULL, 1, NULL),
(66, 6, '10:15:00', '10:15:00', 0, NULL),
(67, 2, NULL, NULL, 1, NULL),
(68, 6, '10:15:00', '10:15:00', 0, NULL),
(69, 2, NULL, NULL, 1, NULL),
(70, 6, '10:15:00', '10:15:00', 0, NULL),
(71, 2, NULL, NULL, 1, NULL),
(72, 6, '10:30:00', '10:30:00', 0, NULL),
(73, 0, NULL, NULL, 1, NULL),
(74, 1, NULL, NULL, 1, NULL),
(75, 2, NULL, NULL, 1, NULL),
(76, 3, NULL, NULL, 1, NULL),
(77, 4, NULL, NULL, 1, NULL),
(78, 5, '07:00:00', '13:00:00', 0, NULL),
(79, 6, '07:00:00', '13:00:00', 0, NULL),
(80, 0, NULL, NULL, 1, NULL),
(81, 1, NULL, NULL, 1, NULL),
(82, 4, '09:00:00', '13:00:00', 0, NULL),
(83, 0, NULL, NULL, 1, NULL),
(84, 1, NULL, NULL, 1, NULL),
(85, 4, '09:00:00', '13:00:00', 0, NULL),
(86, 0, NULL, NULL, 1, NULL),
(87, 1, NULL, NULL, 1, NULL),
(88, 4, '09:00:00', '13:00:00', 0, NULL),
(89, 0, NULL, NULL, 1, NULL),
(90, 1, NULL, NULL, 1, NULL),
(91, 4, '09:00:00', '13:00:00', 0, NULL),
(92, 0, NULL, NULL, 1, NULL),
(93, 1, NULL, NULL, 1, NULL),
(94, 4, '09:00:00', '13:00:00', 0, NULL),
(95, 0, NULL, NULL, 1, NULL),
(96, 1, NULL, NULL, 1, NULL),
(97, 4, '09:00:00', '13:00:00', 0, NULL),
(98, 0, NULL, NULL, 1, NULL),
(99, 1, NULL, NULL, 1, NULL),
(100, 4, '09:00:00', '13:00:00', 0, NULL),
(101, 0, NULL, NULL, 1, NULL),
(102, 1, NULL, NULL, 1, NULL),
(103, 4, '09:00:00', '13:00:00', 0, NULL),
(104, 0, NULL, NULL, 1, NULL),
(105, 1, NULL, NULL, 1, NULL),
(106, 2, NULL, NULL, 1, NULL),
(107, 3, NULL, NULL, 1, NULL),
(108, 4, NULL, NULL, 1, NULL),
(109, 5, '07:00:00', '13:00:00', 0, NULL),
(110, 6, '07:00:00', '13:00:00', 0, NULL),
(111, 0, NULL, NULL, 1, 71),
(112, 1, NULL, NULL, 1, 71),
(113, 2, NULL, NULL, 1, 71),
(114, 3, NULL, NULL, 1, 71),
(115, 4, NULL, NULL, 1, 71),
(116, 5, '07:00:00', '13:00:00', 0, 71),
(117, 6, '07:00:00', '13:00:00', 0, 71),
(118, 0, NULL, NULL, 1, NULL),
(119, 1, NULL, NULL, 1, NULL),
(120, 5, '12:00:00', '17:45:00', 0, NULL),
(121, 5, '12:00:00', '17:45:00', 0, NULL),
(122, 5, '12:00:00', '17:45:00', 0, NULL),
(123, 5, '12:00:00', '17:45:00', 0, NULL),
(124, 5, '12:00:00', '17:45:00', 0, NULL),
(125, 5, '12:00:00', '17:45:00', 0, NULL),
(126, 5, '12:00:00', '17:45:00', 0, NULL),
(127, 5, '12:00:00', '17:45:00', 0, NULL),
(128, 5, '12:00:00', '17:45:00', 0, NULL),
(129, 5, '12:00:00', '17:45:00', 0, NULL),
(131, 0, '05:18:00', '06:00:00', 0, NULL),
(133, 0, NULL, NULL, 1, 77),
(134, 1, '13:00:00', '13:00:00', 0, 77),
(139, 0, '05:18:00', '06:00:00', 0, 1),
(140, 5, '12:00:00', '17:45:00', 0, 75);

-- --------------------------------------------------------

--
-- Struktura tabele `working_types`
--

CREATE TABLE `working_types` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_slovenian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_slovenian_ci;

--
-- Odloži podatke za tabelo `working_types`
--

INSERT INTO `working_types` (`id`, `type`) VALUES
(1, 'Open on selected hours'),
(2, 'Always open'),
(3, 'No hours available'),
(4, 'Permanently closed');

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingId` (`parkingId`);

--
-- Indeksi tabele `area_occupancies`
--
ALTER TABLE `area_occupancies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `areaId` (`areaId`);

--
-- Indeksi tabele `fee_types`
--
ALTER TABLE `fee_types`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingId` (`parkingId`);

--
-- Indeksi tabele `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `parkings`
--
ALTER TABLE `parkings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingTypeId` (`parkingTypeId`),
  ADD KEY `parkingCategoryId` (`parkingCategoryId`),
  ADD KEY `workingTypeId` (`workingTypeId`),
  ADD KEY `feeTypeId` (`feeTypeId`),
  ADD KEY `addressId` (`addressId`);

--
-- Indeksi tabele `parking_categories`
--
ALTER TABLE `parking_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `parking_fee_by_days`
--
ALTER TABLE `parking_fee_by_days`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingId` (`parkingId`);

--
-- Indeksi tabele `parking_manager`
--
ALTER TABLE `parking_manager`
  ADD PRIMARY KEY (`managerId`,`parkingId`),
  ADD KEY `parkingId` (`parkingId`);

--
-- Indeksi tabele `parking_occupancies`
--
ALTER TABLE `parking_occupancies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingId` (`parkingId`);

--
-- Indeksi tabele `parking_provider`
--
ALTER TABLE `parking_provider`
  ADD PRIMARY KEY (`providerId`,`parkingId`),
  ADD KEY `parkingId` (`parkingId`);

--
-- Indeksi tabele `parking_types`
--
ALTER TABLE `parking_types`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- Indeksi tabele `slots`
--
ALTER TABLE `slots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `areaId` (`areaId`);

--
-- Indeksi tabele `working_hours`
--
ALTER TABLE `working_hours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingId` (`parkingId`);

--
-- Indeksi tabele `working_types`
--
ALTER TABLE `working_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT zavrženih tabel
--

--
-- AUTO_INCREMENT tabele `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT tabele `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT tabele `area_occupancies`
--
ALTER TABLE `area_occupancies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=477;
--
-- AUTO_INCREMENT tabele `fee_types`
--
ALTER TABLE `fee_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT tabele `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT tabele `managers`
--
ALTER TABLE `managers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT tabele `parkings`
--
ALTER TABLE `parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT tabele `parking_categories`
--
ALTER TABLE `parking_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT tabele `parking_fee_by_days`
--
ALTER TABLE `parking_fee_by_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT tabele `parking_occupancies`
--
ALTER TABLE `parking_occupancies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT tabele `parking_types`
--
ALTER TABLE `parking_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT tabele `providers`
--
ALTER TABLE `providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT tabele `slots`
--
ALTER TABLE `slots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT tabele `working_hours`
--
ALTER TABLE `working_hours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT tabele `working_types`
--
ALTER TABLE `working_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Omejitve tabel za povzetek stanja
--

--
-- Omejitve za tabelo `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`parkingId`) REFERENCES `parkings` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omejitve za tabelo `area_occupancies`
--
ALTER TABLE `area_occupancies`
  ADD CONSTRAINT `area_occupancies_ibfk_1` FOREIGN KEY (`areaId`) REFERENCES `areas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omejitve za tabelo `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `locations_ibfk_1` FOREIGN KEY (`parkingId`) REFERENCES `parkings` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omejitve za tabelo `parkings`
--
ALTER TABLE `parkings`
  ADD CONSTRAINT `fk_parking_address` FOREIGN KEY (`addressId`) REFERENCES `addresses` (`id`),
  ADD CONSTRAINT `fk_parking_category` FOREIGN KEY (`parkingCategoryId`) REFERENCES `parking_categories` (`id`),
  ADD CONSTRAINT `fk_parking_fee_type` FOREIGN KEY (`feeTypeId`) REFERENCES `fee_types` (`id`),
  ADD CONSTRAINT `fk_parking_type` FOREIGN KEY (`parkingTypeId`) REFERENCES `parking_types` (`id`),
  ADD CONSTRAINT `fk_parking_working_type` FOREIGN KEY (`workingTypeId`) REFERENCES `working_types` (`id`);

--
-- Omejitve za tabelo `parking_fee_by_days`
--
ALTER TABLE `parking_fee_by_days`
  ADD CONSTRAINT `parking_fee_by_days_ibfk_1` FOREIGN KEY (`parkingId`) REFERENCES `parkings` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omejitve za tabelo `parking_manager`
--
ALTER TABLE `parking_manager`
  ADD CONSTRAINT `parking_manager_ibfk_1` FOREIGN KEY (`managerId`) REFERENCES `managers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parking_manager_ibfk_2` FOREIGN KEY (`parkingId`) REFERENCES `parkings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omejitve za tabelo `parking_occupancies`
--
ALTER TABLE `parking_occupancies`
  ADD CONSTRAINT `parking_occupancies_ibfk_1` FOREIGN KEY (`parkingId`) REFERENCES `parkings` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omejitve za tabelo `parking_provider`
--
ALTER TABLE `parking_provider`
  ADD CONSTRAINT `parking_provider_ibfk_1` FOREIGN KEY (`providerId`) REFERENCES `providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parking_provider_ibfk_2` FOREIGN KEY (`parkingId`) REFERENCES `parkings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omejitve za tabelo `slots`
--
ALTER TABLE `slots`
  ADD CONSTRAINT `slots_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `slots_ibfk_2` FOREIGN KEY (`areaId`) REFERENCES `areas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omejitve za tabelo `working_hours`
--
ALTER TABLE `working_hours`
  ADD CONSTRAINT `working_hours_ibfk_1` FOREIGN KEY (`parkingId`) REFERENCES `parkings` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
