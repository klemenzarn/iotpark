/*BOOTSTRAP - loading env vars, init log service*/
//setting .env variables
require('dotenv').config();


var Log = require('./app/core/Log');
var WinstonLogger = require('./app/core/logger/WinstonLogger');
var Sequelize = require('sequelize');
var DBConfig = require('./app/config/database');

var winston = require('winston');
require('winston-loggly-bulk');


//init logger
global.winston = winston;
Log.init(new WinstonLogger());
global.Log = Log;
global.Sequelize = Sequelize;

const sequelize = new Sequelize(DBConfig.database, DBConfig.username, DBConfig.password, DBConfig.connection);
global.sequelize = sequelize;

global.asyncWrap = fn => {
    return (req, res, next) => {
        fn(req, res, next).catch(next);
    };
};