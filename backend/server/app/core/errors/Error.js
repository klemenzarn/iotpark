var Log = require('../Log');

var defaultErrorData = {
    status: 500,
    errorMessage: 'Server error',
    details: 'Internal server error happened',
    customError: true
};

class Error {

    constructor(error) {
        var errorData = defaultErrorData;
        if (this.isCustomError(error)) {
            errorData = error;
        } else {
            errorData.details = error;
        }

        this.status = errorData.status;
        this.errorMessage = errorData.errorMessage;
        this.details = errorData.details;
        this.customError = errorData.customError;
    }

    isCustomError(error) {
        return error && error.customError == true;
    }

    outputData() {
        return {
            status: this.status,
            errorMessage: this.errorMessage,
            error: this.error,
            details: this.getDetails()
        }
    }

    getDetails() {
        if (this.hasStack()) {
            return this.details.stack;
        } else {
            return this.details;
        }
    }

    hasStack() {
        return this.details && this.details.stack != undefined;
    }
}

module.exports = Error;