var Error = require('./Error');

class ForbiddenError extends Error {
    constructor() {
        super();
        this.status = 403;
        this.errorMessage = "Forbidden"
        this.details = 'Forbidden access, check your auth params for this source';
    }

}

module.exports = ForbiddenError;