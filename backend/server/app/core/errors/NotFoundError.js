var Error = require('./Error');

class NotFoundError extends Error {
    constructor() {
        super();
        this.status = 404;
        this.errorMessage = "Not found"
        this.details = 'Record not found in database, check params.';
    }

}

module.exports = NotFoundError;