class Request {

    static getIp(req) {
        try {
            return req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress ||
                req.connection.socket.remoteAddress;
        } catch (err) {
            return 'ip uknown';
        }
    }
}

module.exports = Request;