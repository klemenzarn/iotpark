var Logger = require('./Logger');
var simpleNodeLogger = require('simple-node-logger');
var LogLevels = require('./LogLevels');


class FileLogger extends Logger {

    constructor(logFileName = 'logs/server.log', consoleLogEnabled = false) {
        super();

        this.consoleLogEnabled = consoleLogEnabled;
        this.logger = simpleNodeLogger.createSimpleFileLogger(logFileName);
    }

    log(level, msg, logData) {
        let logOutputMessage = this.generateLogMessage(msg, logData);

        switch (level) {
            case LogLevels.info:
                this.logger.info(logOutputMessage);
                break;
            case LogLevels.error:
                this.logger.error(logOutputMessage);
                break;
            case LogLevels.warn:
                this.logger.warn(logOutputMessage);
                break;
        }

        if (this.consoleLogEnabled) {
            console.log(level + ': ' + logOutputMessage);
        }
    }

    generateLogMessage(msg, logData) {
        let logMessage = msg;

        if (this.hasLogData(logData)) {
            logMessage += ', additional data: ' + this.serealizeLogData(logData);
        }

        return logMessage;
    }

    hasLogData(logData) {
        return logData != null && logData != undefined && Object.keys(logData).length !== 0;
    }

    serealizeLogData(logData) {
        return JSON.stringify(logData);
    }

}

module.exports = FileLogger;