var Logger = require('./Logger');
var LogLevels = require('./LogLevels');
var Server = require('../Server');

class WinstonLogger extends Logger {

    constructor() {
        super();

        this.loggySereviceInited = false;

        if (Server.isInProduction()) {

            //add Loggy transport so winston can send data to Loggy.com
            winston.add(winston.transports.Loggly, {
                token: "3aa53d2c-1b48-4bd7-8835-9bc7da2e3de3",
                subdomain: "klemenzarn",
                tags: ["Winston-NodeJS"],
                json: true
            });

            this.loggySereviceInited = true;
        }
    }


    log(level, msg, logData) {
        winston.log(level, msg, logData);

        if (this.loggySereviceInited == false && Server.isInProduction()) {
            winston.log(LogLevels.error, 'Loggy not inited, did you forget to call Log.init()? Logs will not be logged to Loggy.com service.');
        }
    }
}

module.exports = WinstonLogger;