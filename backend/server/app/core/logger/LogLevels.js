var LogLevels = {
    info: 'info',
    warn: 'warn',
    error: 'error'
}

module.exports = LogLevels;