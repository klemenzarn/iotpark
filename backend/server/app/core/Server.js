var ServerStates = require('./ServerStates');

class Server {

    static isInProduction() {
        return process.env.ENV == ServerStates.prod;
    }

    static isInDevelopment() {
        return process.env.ENV == ServerStates.dev;
    }
}

module.exports = Server;