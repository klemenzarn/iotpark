var Models = require('../../model/models');

class ParkingCreator {

    constructor(postBody) {
        this.data = postBody;
    }

    async save() {
        //return await this.create();
        return this.data.parkingId ? await this.edit() : await this.create();
    }


    async create() {
        let parkingFee = 0;
        if (this.data.feePerHour) {
            parkingFee = this.data.feePerHour;
        }

        let parkingFeeTypeId = 2;
        if (this.data.parkingFeeVaryByDay == 'on') {
            parkingFeeTypeId = 3;
        } else if (this.data.freeParking == 'on') {
            parkingFeeTypeId = 1;
        }

        let parkingTypeId = null;
        if (this.data.type != -1) {
            parkingTypeId = this.data.type;
        }

        let parkingCategoryId = null;
        if (this.data.category != -1) {
            parkingCategoryId = this.data.category;
        }

        //creating address this.data for parking
        let parkingAddress = await Models.Address.create({
            addressName: this.data.address,
            zip: this.data.zip,
            city: this.data.city,
        });


        let parkingInsertData = {
            name: this.data.name,
            hash: this.data.hash,
            hasSlotdata: false,
            parkingTypeId: parkingTypeId,
            parkingCategoryId: parkingCategoryId,
            workingTypeId: this.data.openParkingType,
            phone: this.data.phone,
            webSite: this.data.website,
            fee: parkingFee,
            feeTypeId: parkingFeeTypeId,
            published: this.data.published,
            image: this.data.parkingImage,
            addressId: parkingAddress.id
        };

        //creating parking
        let parking = await Models.Parking.create(parkingInsertData);

        //create location
        await Models.Location.create({
            lat: this.data.lat,
            lng: this.data.lng,
            parkingId: parking.id
        });

        //creating this.data for each day in week and hour for fee and opening hours
        let workingHoursKey = 'workingHoursDay_';
        var parkingFeeKey = 'parkingFeeDay_';

        await this.addWorkingHoursAndFeesByDay(parking.id);

        //inserting selected provider
        let provider = await Models.Provider.findOne({ where: { id: this.data.provider } });
        await parking.addProvider(provider);

        //inserting selected manager
        let manager = await Models.Manager.findOne({ where: { id: this.data.manager } });
        await parking.addManager(manager);

        if (this.data.availableParkings) {
            await Models.ParkingOccupancy.create({
                capacity: this.data.availableParkings,
                parkingId: parking.id
            });
        }

        return parking.id;
    }

    //neb smel bit tak težko -> recimo parking.address.update({data});
    async edit() {
        let parkingFee = 0;
        if (this.data.feePerHour) {
            parkingFee = this.data.feePerHour;
        }

        let parkingFeeTypeId = 2;
        if (this.data.parkingFeeVaryByDay == 'on') {
            parkingFeeTypeId = 3;
        } else if (this.data.freeParking == 'on') {
            parkingFeeTypeId = 1;
        }

        let parkingTypeId = null;
        if (this.data.type != -1) {
            parkingTypeId = this.data.type;
        }

        let parkingCategoryId = null;
        if (this.data.category != -1) {
            parkingCategoryId = this.data.category;
        }

        let parking = await Models.Parking.overallOne({ id: this.data.parkingId });

        await parking.address.update({
            addressName: this.data.address,
            zip: this.data.zip,
            city: this.data.city
        });

        await parking.location.update({
            lat: this.data.lat,
            lng: this.data.lng
        });

        await Models.WorkingHour.destroy({ where: { parkingId: parking.id } });
        await Models.FeeByDays.destroy({ where: { parkingId: parking.id } });
        await parking.removeProvider(parking.providers);
        await parking.removeManager(parking.managers);

        await this.addWorkingHoursAndFeesByDay(parking.id);

        //inserting selected provider
        let provider = await Models.Provider.findOne({ where: { id: this.data.provider } });
        await parking.addProvider(provider);

        //inserting selected manager
        let manager = await Models.Manager.findOne({ where: { id: this.data.manager } });
        await parking.addManager(manager);
        
         if (this.data.availableParkings && parking.parking_occupancy) {
            await parking.parking_occupancy.update({
                capacity: this.data.availableParkings
            });
        }

        let parkingInsertData = {
            name: this.data.name,
            hash: this.data.hash,
            hasSlotdata: false,
            parkingTypeId: parkingTypeId,
            parkingCategoryId: parkingCategoryId,
            workingTypeId: this.data.openParkingType,
            phone: this.data.phone,
            webSite: this.data.website,
            fee: parkingFee,
            feeTypeId: parkingFeeTypeId,
            published: this.data.published,
            image: this.data.parkingImage
        };

        //update parking
        let parkingUpdate = await parking.update(parkingInsertData);


        return parkingUpdate.id;
    }

    //creating data for each day in week and hour for fee and opening hours
    async addWorkingHoursAndFeesByDay(parkingId) {
        let workingHoursKey = 'workingHoursDay_';
        var parkingFeeKey = 'parkingFeeDay_';

        for (let i = 0; i < 7; i++) {
            if (this.data[workingHoursKey + i] == 'on') {
                let alwaysOpen = this.data[workingHoursKey + i + '_alwaysOpen'] == 'on';
                let start = this.data[workingHoursKey + i + '_start'];
                let end = this.data[workingHoursKey + i + '_end'];


                await Models.WorkingHour.create({
                    day: i,
                    allDay: alwaysOpen,
                    start: start,
                    end: end,
                    parkingId: parkingId
                })
            }

            if (this.data.parkingFeeVaryByDay == 'on' && this.data[parkingFeeKey + i] == 'on') {
                let fee = this.data[parkingFeeKey + i + '_fee'];
                try {
                    fee = parseFloat(fee);
                } catch (err) {
                    fee = 0;
                }

                let start = this.data[parkingFeeKey + i + '_start'];
                let end = this.data[parkingFeeKey + i + '_end'];

                await Models.FeeByDays.create({
                    day: i,
                    fee: fee,
                    start: start,
                    end: end,
                    parkingId: parkingId
                });
            }
        }
    }

}


module.exports = ParkingCreator;