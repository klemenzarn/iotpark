var Log = require('./Log');
var Request = require('./Request');

module.exports = asyncWrap(async(req, res, next) => {
    let allowed = true; //TODO

    if (allowed) {
        Log.info('request happened on admin', {
            url: req.url,
            ip: Request.getIp(req)
        });

        next();
    } else {
        res.redirect('/login');
    }
});