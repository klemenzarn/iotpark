var multer = require('multer')
var mime = require('mime');
var sanitize = require("sanitize-filename");

var parkingImagesStore = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'admin/public/img/content')
    },
    filename: function(req, file, cb) {
        var filename;

        try {
            if (req.body && req.body.name) {
                filename = req.body.name;
            } else {
                filename = file.filename;
            }
        } catch (err) {
            filename = 'parking-image';
        }

        filename = sanitize(filename).replace(' ', '-');

        cb(null, filename + '-' + Date.now() + '.' + mime.extension(file.mimetype));
    }
});

module.exports = multer({ storage: parkingImagesStore });