var Server = require('./Server');
var LogLevels = require('./logger/LogLevels');


let logger = null;

class Log {

    static init(loggerProvider) {
        logger = loggerProvider;
    }

    static info(msg, metaData = {}) {
        this.write(msg, metaData, LogLevels.info);
    }

    static warn(msg, metaData = {}) {
        this.write(msg, metaData, LogLevels.warn);
    }

    static error(msg, metaData = {}) {
        this.write(msg, metaData, LogLevels.error);

        //todo email notification???
    }

    static write(msg, metaData = {}, level = LogLevels.info) {
        logger.log(level, msg, metaData);
    }
}

module.exports = Log;