var express = require('express');

var router = express.Router();
var ForbiddenError = require('../../core/errors/ForbiddenError');
var NotFoundError = require('../../core/errors/NotFoundError');
var Error = require('../../core/errors/Error');
var Log = require('../../core/Log');
var Request = require('../../core/Request');
var fs = require('fs');

var ParkingCreator = require('../../core/admin/ParkingCreator');
var Models = require('../../model/models');
var Parking = Models.Parking;
var Area = Models.Area;
var Slot = Models.Slot;
var AreaOccupancy = Models.AreaOccupancy;
var ParkingOccupancy = Models.ParkingOccupancy;
var Address = Models.Address;

var uploadParkingImage = require('../../core/file_upload/parking_image_upload');

router.use(asyncWrap(async(req, res, next) => {
    Log.info('request happened on api v1', {
        url: req.url,
        ip: Request.getIp(req)
    });

    next();
}));


router.get('/parking', asyncWrap(async(req, res, next) => {
    let parkingsData = await Parking.overallAll({ published: true });
    res.json(parkingsData);
}));

router.get('/parking/:id', asyncWrap(async(req, res, next) => {
    let parkingsData = await Parking.overallOne({ id: req.params.id, published: true });

    if (parkingsData) {
        res.json(parkingsData);
    } else {
        next(new NotFoundError());
    }
}));

router.post('/parking/add', uploadParkingImage.single('parking_image'), asyncWrap(async(req, res, next) => {
    let data = req.body;
    data.parkingImage = null;
    if (req.file && req.file.filename) {
        data.parkingImage = req.file.filename;
    }

    let parkingCreator = new ParkingCreator(data);
    let parkingId = await parkingCreator.save();

    if (req.body.redirect) {
        res.redirect('/parking');
    } else {
        res.json(await Parking.overallOne({ id: parkingId }));
    }
}));

//TODO REFACTOR
router.post('/parking/edit', uploadParkingImage.single('parking_image'), asyncWrap(async(req, res, next) => {
    let data = req.body;
    if (req.file && req.file.filename) {
        data.parkingImage = req.file.filename;
    } else {
        data.parkingImage = data.existingImage;
    }

    try {
        var parkingCreator = new ParkingCreator(data);
        await parkingCreator.save();
    } catch (err) {
        console.log('error editing', err);
        req.flash('error', 'edit_parking_error');
    }

    res.redirect('/parking/edit/' + data.parkingId);
}));

router.delete('/parking/:id', asyncWrap(async(req, res, next) => {
    let id = parseInt(req.params.id);
    let nowAllowedDeleteId = [1, 2];
    if (nowAllowedDeleteId.includes(id)) {
        res.send('not allowed');
        return;
    }

    let destroyedRows = await Parking.destroy({ where: { id: id } });
    res.send('deleted rows: ' + destroyedRows);
}));

router.get('/parking/delete/:id', asyncWrap(async(req, res, next) => {
    let id = parseInt(req.params.id);
    let nowAllowedDeleteId = [1, 2];
    if (nowAllowedDeleteId.includes(id)) {
        res.send('not allowed');
        return;
    }

    await Parking.destroy({ where: { id: id } });

    let backURL = req.header('Referer') || '/';
    res.redirect(backURL);
}));

//TODO REFACTOR
async function insertOccupancyData(parking, occupancyData) {

    //če obstaja podatek o frej prostorih je potrebno izračunati zasedenost parkirnih mest iz podatka kapaciteta... 
    //Ali je ta podana, ali pa se dobi podatek iz baze, kjer urednik statično zapiše kapaciteto parkirišča
    if (occupancyData.fre) {
        if (occupancyData.cap == 0) {


            if (parking.parking_occupancy != null) {
                occupancyData.cap = parking.parking_occupancy.capacity;
            }
        }

        if (occupancyData.cap != 0) {
            occupancyData.occ = occupancyData.cap - occupancyData.fre;
            if (occupancyData.occ < 0) {
                console.log('zasdednost je negativna, verjetno je napaka v kapaciteti parkirišča');
                return;
            }
        } else {
            console.log('ni dovolj podatkov za izračun occupancy');
            return;
        }
    }

    let m = await ParkingOccupancy.findCreateUpdate({
        parkingId: parking.id
    }, {
        capacity: occupancyData.cap,
        occupancy: occupancyData.occ,
        parkingId: parking.id
    });

    //najdemo vse area-je oz nadstropje po pridobljenem hashu
    let areasByHashQuery = occupancyData.flo.map(floorData => {
        return Area.findOne({
            where: {
                hash: floorData.hash
            }
        });
    });

    let areasFoundByHash = await Promise.all(areasByHashQuery);

    let insertAreaOccupancyQuery = areasFoundByHash.map((area, index) => {
        let areaOccupancy = occupancyData.flo[index];

        if (area != null) {
            return AreaOccupancy.findCreateUpdate({
                areaId: area.id
            }, {
                capacity: areaOccupancy.cap,
                occupancy: areaOccupancy.occ,
                areaId: area.id
            });
        } else {
            console.log(`area with hash '${areaOccupancy.hash}' not in database`);
            return null;
        }

    }).filter(promiseCall => { return promiseCall != null });

    await Promise.all(insertAreaOccupancyQuery);

    let overallParkingData = await Parking.overallAll({ id: parking.id });

    return ({
        parkingDataAfterInsert: overallParkingData,
        parkingDataToInsert: occupancyData
    });
}

router.get('/test', asyncWrap(async(req, res, next) => {
    var fileName = 'da_vidimo.json';
    fs.readFile(fileName, 'utf8', (err, data) => {
        if (err) throw err;

        res.json(JSON.parse(data));

    });
}));

router.post('/parking', asyncWrap(async(req, res, next) => {

    /*var fileName = 'da_vidimo.json';
    var dataTest = fs.readFileSync(fileName, 'utf8');

    let reqBody = JSON.parse(dataTest); //req.body;*/

    let reqBody = req.body;

    var fileName = new Date().getTime().toString() + '.json';
    var json = JSON.stringify(reqBody);

    fs.writeFile('../parkingDataDump/' + fileName, json, 'utf8', error => {
        if (error)
            console.log('error happened', error);
        else
            console.log('data dumped in ', fileName, 'ok!');
    });

    let actualData = reqBody.data;

    //creating promise array
    let findParkingsPromiseArray = actualData.map(parkingData => {
        return Parking.findOne({ where: { hash: parkingData.hash }, include: { model: Models.ParkingOccupancy } });
    });

    let parkingsArray = await Promise.all(findParkingsPromiseArray);

    let promiseQueryArray = parkingsArray.map((parking, index) => {
        let parkingOccupancy = actualData[index];
        if (parking != null) {
            return insertOccupancyData(parking, parkingOccupancy);
        } else {
            console.log(`parking with hash '${parkingOccupancy.hash}' not in database`);
            return null;
        }
    }).filter(promiseCall => { return promiseCall != null });

    let insertionResults = await Promise.all(promiseQueryArray);

    res.json({
        insertionResults: insertionResults
    });
}));


router.get('/area/:id', asyncWrap(async(req, res, next) => {
    let area = await Area.findOne({
        where: { id: req.params.id },
        include: [
            { model: Slot },
            { model: AreaOccupancy }
        ],
    });

    if (area) {
        res.json(area);
    } else {
        next(new NotFoundError());
    }

}));


module.exports = router;