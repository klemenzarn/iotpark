var Database = require('../models/database');
var Parking = require('../models/parking');
var Area = require('../models/Area');
var Slot = require('../models/Slot');

Database.init();

//seed data - SLOT
var parkingId = 2;
var slotsNumber = 500;


function getRandomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function getRandomOccupacy() {
    return Math.floor(Math.random() * 2) == 1 ? true : false
}

function getRandomName() {
    var text = '';
    //var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var possible = '0123456789';

    for (var i = 0; i < 2; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    text += '-';

    for (var i = 0; i < 2; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


Area.findByParkingId(parkingId)
    .then(areas => {
        let areaIdList = [];
        areas.forEach(area => {
            areaIdList.push(area.ID);
        });

        for (let i = 0; i < slotsNumber; i++) {
            var areaId = areaIdList[Math.floor(Math.random() * areaIdList.length)];
            var occupacy = getRandomOccupacy();
            var nameId = getRandomName();
            var date = getRandomDate(new Date(2016, 0, 1), new Date());
            var seedData = [nameId, occupacy, areaId, date];
            console.log(seedData);

            Database.insert(
                `
                INSERT INTO slot (name, occupied, area_id, date_changed) 
                VALUES (?, ?, ?, ?)
                `, seedData
            );
        }
    })
    .catch(error => {
        console.log(error)
    });