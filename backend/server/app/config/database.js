var extend = require('node.extend');

var defaultConfig = {
    username: 'root',
    password: '',
    database: 'IoTPark_new',
    connection: {
        host: 'localhost',
        dialect: 'mysql',
        logging: false,
        pool: {
            max: 100,
            min: 0,
            idle: 10000
        },
    }
};

var Configs = {
    dev: {},

    prod: {
        username: 'iotpark',
        password: 'iotpark_v3rsO',
        database: 'iotpark'
    },

    staging: {
        username: 'iotpark',
        password: 'iotpark_staging',
        database: 'iotpark'
    },

    now: {
        username: '',
        password: '',
        database: '',
        connection: {
            host: 'localhost',
            dialect: 'sqlite',
            logging: false,
            syncOnAssociation: false,
            pool: {
                max: 1000,
                min: 0,
                idle: 10000
            },


            storage: 'database/iotParkSqlite.sqlite'
        }
    },
};

var selectedConfig = Configs[process.env.ENV]; //change in .env file

module.exports = extend(defaultConfig, selectedConfig);