const WorkingHour = sequelize.define('working_hour', {
    day: Sequelize.INTEGER,
    start: Sequelize.TIME,
    end: Sequelize.TIME,
    allDay: { type: Sequelize.BOOLEAN, defaultValue: false, allowNull: false },
}, {
    timestamps: false,
});


module.exports = WorkingHour;