var Area = require('./area');

const AreaOccupancy = sequelize.define('area_occupancy', {
    capacity: Sequelize.INTEGER,
    occupancy: Sequelize.INTEGER
}, {
    timestamps: false,
});

module.exports = AreaOccupancy;