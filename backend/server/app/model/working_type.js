const WorkingType = sequelize.define('working_type', {
    type: Sequelize.STRING,
}, {
    timestamps: false,
});


module.exports = WorkingType;