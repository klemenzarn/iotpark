const Location = sequelize.define('location', {
    lat: Sequelize.DOUBLE,
    lng: Sequelize.DOUBLE,
    polyline: Sequelize.TEXT
}, {
    timestamps: false,
});


module.exports = Location;