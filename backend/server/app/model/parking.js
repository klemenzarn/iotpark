const Parking = sequelize.define('parking', {
    name: Sequelize.STRING,
    hash: Sequelize.STRING,
    hasSlotData: Sequelize.BOOLEAN,
    published: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false },
    phone: Sequelize.STRING,
    webSite: Sequelize.STRING,
    fee: { type: Sequelize.DOUBLE, allowNull: false, defaultValue: 0 },
    image: Sequelize.STRING
}, {
    timestamps: false,
});



module.exports = Parking;