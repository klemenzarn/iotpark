var Parking = require('./parking');

const ParkingOccupancy = sequelize.define('parking_occupancy', {
    capacity: Sequelize.INTEGER,
    occupancy: Sequelize.INTEGER
}, {
    timestamps: false,
});

module.exports = ParkingOccupancy;