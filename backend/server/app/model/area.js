const Area = sequelize.define('area', {
    shortName: Sequelize.STRING,
    fullName: Sequelize.STRING,
    hash: Sequelize.STRING
}, {
    timestamps: false,
});


module.exports = Area;