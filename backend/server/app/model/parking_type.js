const ParkingType = sequelize.define('parking_type', {
    type: Sequelize.STRING,
}, {
    timestamps: false,
});


module.exports = ParkingType;