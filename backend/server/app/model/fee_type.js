const FeeType = sequelize.define('fee_type', {
    type: Sequelize.STRING,
}, {
    timestamps: false,
});


module.exports = FeeType;