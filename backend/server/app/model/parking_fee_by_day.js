const FeeByDays = sequelize.define('parking_fee_by_day', {
    day: Sequelize.INTEGER,
    start: Sequelize.TIME,
    end: Sequelize.TIME,
    fee: Sequelize.DOUBLE
}, {
    timestamps: false,
});


module.exports = FeeByDays;