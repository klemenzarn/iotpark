var Parking = require('./parking');
var Area = require('./area');
var Slot = require('./slot');
var AreaOccupancy = require('./area_occupancy');
var ParkingOccupancy = require('./parking_occupancy');
var Address = require('./address');
var ParkingType = require('./parking_type');
var ParkingCategory = require('./parking_category');
var Location = require('./location')
var Provider = require('./provider')
var Manager = require('./manager')
var WorkingType = require('./working_type')
var WorkingHour = require('./working_hour')
var FeeType = require('./fee_type')
var FeeByDays = require('./parking_fee_by_day')

Parking.hasMany(Area);
Parking.hasOne(ParkingOccupancy);

Parking.belongsTo(Address);

Parking.belongsTo(ParkingType);
Parking.belongsTo(ParkingCategory);
Parking.hasOne(Location);
Parking.belongsTo(WorkingType);
Parking.belongsTo(FeeType);
Parking.hasMany(WorkingHour);
Parking.hasMany(FeeByDays);


WorkingHour.belongsTo(Parking);
FeeByDays.belongsTo(Parking);

// ParkingType.hasOne(Parking);
// ParkingCategory.hasOne(Parking);
// Location.hasOne(Parking);
Manager.belongsToMany(Parking, { through: 'parking_manager' });
Parking.belongsToMany(Manager, { through: 'parking_manager' });

Provider.belongsToMany(Parking, { through: 'parking_provider' });
Parking.belongsToMany(Provider, { through: 'parking_provider' });

Area.belongsTo(Parking);
Area.hasOne(AreaOccupancy);
Area.hasMany(Slot);

Slot.belongsTo(Area);

(async() => {

    //await sequelize.sync();
    // await WorkingType.create({ type: 'Open on selected hours' });
    // await WorkingType.create({ type: 'Always open' });
    // await WorkingType.create({ type: 'No hours available' });
    // await WorkingType.create({ type: 'Permanently closed' });
    //await Manager.create({ name: 'Versor' });
    /*await Address.create({
        addressName: 'Kočevarjeva ulica 7',
        zip: '2000',
        city: 'Maribor'
    });*/
    /*let parkingType = await ParkingType.create({
        type: 'Public'
    });
    console.log(parkingType.get({ plaint: true }));*/
})();

Sequelize.Model.prototype.findCreateUpdate = function(findWhereMap, newValuesMap) {
    return this.findOrCreate({
        where: findWhereMap,
        defaults: findWhereMap
    }).spread(function(newObj, created) {
        for (var key in newValuesMap) {
            newObj[key] = newValuesMap[key];
        }

        return newObj.save();
    });
};


var includingData = [
    { model: ParkingOccupancy },
    { model: Address },
    { model: Location },
    { model: WorkingType },
    { model: WorkingHour },
    { model: FeeType },
    { model: FeeByDays },
    { model: Manager, through: {} },
    { model: Provider, through: {} },
    { model: ParkingType, attributes: ['id', 'type'] },
    { model: ParkingCategory, attributes: ['id', 'category'] },
    { model: Area, include: [AreaOccupancy] },
];

var parkingColumnsSelect = ['id', 'name','hash', 'hasSlotData', 'fee', 'published', 'phone', 'webSite', 'image'];

Parking.overallAll = async(where = {}) => {
    let parkingsOverallData = await Parking.findAll({
        attributes: parkingColumnsSelect,
        where: where,
        include: includingData
    });

    return parkingsOverallData;
};

Parking.overallOne = async(where) => {
    let parkingOverallData = await Parking.findOne({
        attributes: parkingColumnsSelect,
        where: where,
        include: includingData
    });

    return parkingOverallData;
};

module.exports = {
    Parking: Parking,
    Area: Area,
    Address: Address,
    Slot: Slot,
    ParkingOccupancy: ParkingOccupancy,
    AreaOccupancy: AreaOccupancy,
    ParkingType: ParkingType,
    ParkingCategory: ParkingCategory,
    Provider: Provider,
    Manager: Manager,
    WorkingType: WorkingType,
    WorkingHour: WorkingHour,
    FeeByDays: FeeByDays,
    Location: Location,
    FeeType: FeeType
};