var Area = require('./area');

const Slot = sequelize.define('slot', {
    name: Sequelize.STRING,
    occupied: Sequelize.BOOLEAN,
    area_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Area,
            key: 'id',
        }
    }
}, {
    timestamps: false,
});

module.exports = Slot;