const Address = sequelize.define('address', {
    addressName: Sequelize.STRING,
    zip: Sequelize.STRING,
    city: Sequelize.STRING
}, {
    timestamps: false,
});


module.exports = Address;