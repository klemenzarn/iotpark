const ParkingCategory = sequelize.define('parking_category', {
    category: Sequelize.STRING,
}, {
    timestamps: false,
});


module.exports = ParkingCategory;