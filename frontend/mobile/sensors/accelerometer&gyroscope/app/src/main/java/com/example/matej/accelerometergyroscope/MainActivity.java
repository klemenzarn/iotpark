package com.example.matej.accelerometergyroscope;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private TextView txtAcceleration;
    private TextView txtDirection;

    private String additionalAccelerationInfo;

    private SensorManager sensorMan;
    private Sensor accelerometer, gyroscope;

    private float[] mGravity;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtAcceleration = (TextView) findViewById(R.id.txtAcceleration);
        txtDirection = (TextView) findViewById(R.id.txtDirection);

        sensorMan = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyroscope = sensorMan.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

        sensorMan.registerListener(this, accelerometer,
                1000000);
        sensorMan.registerListener(this, gyroscope,
                1000000);
    }

    private int hitCount = 0;
    private double hitSum = 0;
    private double hitResult = 0;

    private final int SAMPLE_SIZE = 25; // change this sample size as you want, higher is more precise but slow measure.
    private final double THRESHOLD = 0.2; // change this threshold as you want, higher is more spike movement

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            mGravity = sensorEvent.values.clone();
            // Shake detection
            float x = mGravity[0];
            float y = mGravity[1];
            float z = mGravity[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float)Math.sqrt(x * x + y * y + z * z);
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta;

            // Make this higher or lower according to how much
            // motion you want to detect
            if (hitCount <= SAMPLE_SIZE) {
                hitCount++;
                hitSum += Math.abs(mAccel);

                if (mAccel > 0)
                    additionalAccelerationInfo = " with acceleration";
                else
                    additionalAccelerationInfo = " with decceleration";
            } else {
                hitResult = hitSum / SAMPLE_SIZE;

                if (hitResult > THRESHOLD) {
                    txtAcceleration.setText("Moving" + additionalAccelerationInfo);
                    if (additionalAccelerationInfo == " with acceleration")
                        txtAcceleration.setTextColor(Color.GREEN);
                    else
                        txtAcceleration.setTextColor(Color.RED);
                } else {
                    txtAcceleration.setText("Stop");
                    txtAcceleration.setTextColor(Color.BLACK);
                }

                hitCount = 0;
                hitSum = 0;
                hitResult = 0;
            }
        }

        if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            if (y < -1 || x < -1)
            {
                txtDirection.setText("Right");
            }
            if (y > 1 || x > 1)
            {
                txtDirection.setText("Left");
            }
            if ((y > -1 && y < 1) && (x > -1 && x < 1))
            {
                txtDirection.setText("No left, no right");
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorMan.registerListener(this, accelerometer,
                1000000);
        sensorMan.registerListener(this, gyroscope,
                1000000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorMan.unregisterListener(this);
    }
}
