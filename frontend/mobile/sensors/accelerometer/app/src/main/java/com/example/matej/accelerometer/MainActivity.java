package com.example.matej.accelerometer;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    //log tag
    private String LOG_TAG = "AccelometerApp";

    //views, controls,...
    private TextView txtStatus, txtResult, txtAccel, xValue, yValue, zValue, accX, accY, accZ;
    private Button highSensorDelayBtn, lowSensorDelayBtn, stopSensorBtn;
    private Switch switchType;

    //senzor params
    private SensorManager sensorMan;
    private Sensor accelerometer;
    private float[] mGravity;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;
    private int sensorDelay = SensorManager.SENSOR_DELAY_NORMAL;
    private int hitCount = 0;
    private double hitSum = 0;
    private double hitResult = 0;
    private final int SAMPLE_SIZE = 15; // change this sample size as you want, higher is more precise but slow measure.
    private final double THRESHOLD = 0.2; // change this threshold as you want, higher is more spike movement
    private float[] gravityV = new float[3];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // In onCreate method
        sensorMan = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

        startSensor();

        initializeComponents();
    }

    // init buttons, views, ...
    private void initializeComponents() {
        txtStatus = (TextView) findViewById(R.id.txtStatus);
        txtResult = (TextView) findViewById(R.id.txtResult);
        txtAccel = (TextView) findViewById(R.id.txtAccel);

        xValue = (TextView) findViewById(R.id.accelometerX);
        yValue = (TextView) findViewById(R.id.accelometerY);
        zValue = (TextView) findViewById(R.id.accelometerZ);

        accX = (TextView) findViewById(R.id.xAcc);
        accY = (TextView) findViewById(R.id.yAcc);
        accZ = (TextView) findViewById(R.id.zAcc);

        lowSensorDelayBtn = (Button) findViewById(R.id.lowSpeed);
        lowSensorDelayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "low sensor delay");
                setSensorDelay(SensorManager.SENSOR_DELAY_NORMAL);
            }
        });

        highSensorDelayBtn = (Button) findViewById(R.id.highSpeed);
        highSensorDelayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "high sensor delay");
                setSensorDelay(SensorManager.SENSOR_DELAY_GAME);
            }
        });

        stopSensorBtn = (Button) findViewById(R.id.stopSensorBtn);
        stopSensorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopSensor();
            }
        });

        switchType = (Switch) findViewById(R.id.whichWay);
        switchType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                matejWay = isChecked;
                Toast.makeText(getApplicationContext(), matejWay ? "Matej way ON" : "Klemen way ON", Toast.LENGTH_SHORT).show();
            }
        });
        switchType.setChecked(matejWay);
        Toast.makeText(getApplicationContext(), matejWay ? "Matej way ON" : "Klemen way ON", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        startSensor();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopSensor();
    }

    private void stopSensor() {
        sensorMan.unregisterListener(this);
    }

    private void startSensor() {
        sensorMan.registerListener(this, accelerometer, this.sensorDelay);
    }

    private void resetSenzor() {
        stopSensor();
        startSensor();
    }

    private void setSensorDelay(int senzorDelay) {
        this.sensorDelay = senzorDelay;
        resetSenzor();
    }

    long interval = 0, lastEvetn = 0;
    boolean matejWay = false;
    String additionalAccelerationType = "";

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (matejWay) {
                mGravity = sensorEvent.values.clone();
                // Shake detection
                float x = mGravity[0];
                float y = mGravity[1];
                float z = mGravity[2];


                mAccelLast = mAccelCurrent;
                mAccelCurrent = (float) Math.sqrt(x * x + y * y + z * z);
                float delta = mAccelCurrent - mAccelLast;
                mAccel = mAccel * 0.9f + delta;
                Log.d(LOG_TAG, mAccel + "m/s^2");
                // Make this higher or lower according to how much
                // motion you want to detect
                if (hitCount <= SAMPLE_SIZE) {
                    hitCount++;
                    hitSum += Math.abs(mAccel);

                    txtAccel.setText(String.valueOf(mAccel));
                } else {
                    hitResult = hitSum / SAMPLE_SIZE;

                    txtResult.setText(String.valueOf(hitResult));

                    if (hitResult > THRESHOLD) {
                        txtStatus.setText("Moving");
                    } else {
                        txtStatus.setText("Stop");
                    }

                    hitCount = 0;
                    hitSum = 0;
                    hitResult = 0;
                }


                final float alpha = 0.8f;
                //gravity is calculated here
                gravityV[0] = alpha * gravityV[0] + (1 - alpha) * sensorEvent.values[0];
                gravityV[1] = alpha * gravityV[1] + (1 - alpha) * sensorEvent.values[1];
                gravityV[2] = alpha * gravityV[2] + (1 - alpha) * sensorEvent.values[2];
                //acceleration retrieved from the event and the gravity is removed
                float xAcceleration = sensorEvent.values[0] - gravityV[0];
                float yAcceleration = sensorEvent.values[1] - gravityV[1];
                float zAcceleration = sensorEvent.values[2] - gravityV[2];

                renderAccelometerValues(x, y, z);
                renderAccelerationValues(xAcceleration, yAcceleration, zAcceleration);
            } else {
                final float alpha = 0.8f;
                //gravity is calculated here
                gravityV[0] = alpha * gravityV[0] + (1 - alpha) * sensorEvent.values[0];
                gravityV[1] = alpha * gravityV[1] + (1 - alpha) * sensorEvent.values[1];
                gravityV[2] = alpha * gravityV[2] + (1 - alpha) * sensorEvent.values[2];

                //acceleration retrieved from the event and the gravity is removed
                float x = sensorEvent.values[0] - gravityV[0];
                float y = sensorEvent.values[1] - gravityV[1];
                float z = sensorEvent.values[2] - gravityV[2];

                mAccelLast = mAccelCurrent;
                mAccelCurrent = (float) Math.sqrt(x * x + y * y + z * z);
                float delta = mAccelCurrent - mAccelLast;
                mAccel = mAccel * 0.9f + delta;

                Log.d(LOG_TAG, mAccel + "m/s^2");
                // Make this higher or lower according to how much
                // motion you want to detect
                if (hitCount <= SAMPLE_SIZE) {
                    hitCount++;
                    hitSum += Math.abs(mAccel);

                    txtAccel.setText(String.valueOf(mAccel));
                    if(mAccel < 0){
                        additionalAccelerationType = "with deceleration";
                    } else {
                        additionalAccelerationType = "with acceleration";
                    }
                } else {
                    hitResult = hitSum / SAMPLE_SIZE;

                    txtResult.setText(String.valueOf(hitResult));

                    if (hitResult > THRESHOLD) {

                        txtStatus.setText("Moving "+additionalAccelerationType);
                    } else {
                        txtStatus.setText("Stop");
                    }

                    hitCount = 0;
                    hitSum = 0;
                    hitResult = 0;
                }

                renderAccelometerValues(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
                renderAccelerationValues(x, y, z);
            }
        } else if (sensorEvent.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            //testiranje TYPE_LINEAR_ACCELERATION ampak potrebuje gyro
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            if (Math.abs(x) > 3 || Math.abs(y) > 3 || Math.abs(z) > 3) {
                long now = System.currentTimeMillis();
                interval = now - lastEvetn;
                lastEvetn = now;
                Log.d(LOG_TAG, Float.toString(x) + ";" +
                        Float.toString(y) + ";" +
                        Float.toString(z) + ";" +
                        Long.toString(interval) + "\n");
                renderAccelerationValues(x, y, z);
            }
        }
    }

    private void renderAccelometerValues(float x, float y, float z) {
        xValue.setText(String.valueOf(x));
        yValue.setText(String.valueOf(y));
        zValue.setText(String.valueOf(z));
    }

    private void renderAccelerationValues(float x, float y, float z) {
        accX.setText(String.valueOf(x));
        accY.setText(String.valueOf(y));
        accZ.setText(String.valueOf(z));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
