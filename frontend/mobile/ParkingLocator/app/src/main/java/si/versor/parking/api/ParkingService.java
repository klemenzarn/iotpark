package si.versor.parking.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import si.versor.parking.api.models.Area;
import si.versor.parking.api.models.Parking;
import si.versor.parking.api.models.Slot;

/**
 * Created by klemen on 22. 03. 2017.
 */
public interface ParkingService {

    @GET("/api/parking")
    Call<List<Parking>> getParkings();

    @GET("/api/parking/{id}")
    Call<Parking> getParking(@Path("id") int id);

    @GET("/api/area/{id}")
    Call<Area> getArea(@Path("id") int id);

    /*@GET("/api/slot/{id}")
    Call<Slot> getSlot(@Path("id") int id);*/
}
