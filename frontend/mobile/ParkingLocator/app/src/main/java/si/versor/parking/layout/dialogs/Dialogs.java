package si.versor.parking.layout.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import si.versor.parking.R;

/**
 * Created by Klemen on 26. 05. 2017.
 */

public class Dialogs {

    private Activity activity;

    public Dialogs(Activity activity) {
        this.activity = activity;
    }

    public void showConnectionAlert() {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.api_error_title)
                .setMessage(R.string.api_error_text)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    public void showMapOptionsDialog(MapOptionsDialog.OnMapOptionsListener listener) {
        MapOptionsDialog mod = new MapOptionsDialog(activity, listener);
        mod.show();
    }

    public void showParkingOptionsDialog(ParkingOptionsDialog.OnParkingOptionsListener listener) {
        ParkingOptionsDialog mod = new ParkingOptionsDialog(activity, listener);
        mod.show();
    }
}
