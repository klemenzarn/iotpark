package si.versor.parking.core;

/**
 * Created by klemen on 23. 03. 2017.
 */
public class Config {
    public static boolean SECURE_CONNECTION = true;
    public static String LOG_TAG = "IoTPark";
}
