package si.versor.parking.layout;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.versor.parking.R;
import si.versor.parking.api.models.Occupancy;
import si.versor.parking.api.models.Parking;

public class ParkingDetailsActivity extends AppCompatActivity {

    @BindView(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton floatingButton;

    @BindView(R.id.txtViewParkingInformationsTitle) TextView txtViewParkingInformationTitle;
    @BindView(R.id.txtViewParkingInformationsOccupancy) TextView txtViewParkingInformationsOccupancy;
    @BindView(R.id.txtViewParkingInformationsPrice) TextView txtViewParkingInformationsPrice;
    @BindView(R.id.txtViewTravelTime) TextView txtViewTravelTime;

    @BindView(R.id.txtViewParkingAddress) TextView txtViewParkingAddress;
    @BindView(R.id.txtViewParkingPhone) TextView txtViewParkingPhone;
    @BindView(R.id.txtViewParkingWebsite) TextView txtViewParkingWebsite;

    private Parking parkingData;
    private String myLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_details);

        ButterKnife.bind(this);

        initComponents();
    }

    private void initComponents() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout.setTitle(" ");
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);

        parkingData = getIntent().getExtras().getParcelable("parking");
        myLocation = getIntent().getExtras().getString("myLocation");

        // EndCallListener callListener = new EndCallListener();
        // TelephonyManager mTM = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        // mTM.listen(callListener, PhoneStateListener.LISTEN_CALL_STATE);

        displayData();
    }

    private void displayData() {
        Occupancy parkingOccupancy = parkingData.getOccupancy();

        txtViewParkingInformationTitle.setText(parkingData.getName());
        if (parkingOccupancy != null) {
            if (parkingOccupancy.getFreeSlots() > 0) {
                txtViewParkingInformationsOccupancy.setText(String.format(Locale.getDefault(), "%1$d FREE", parkingOccupancy.getFreeSlots()));
                txtViewParkingInformationsOccupancy.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_occupancy_free));
            } else {
                txtViewParkingInformationsOccupancy.setText(getResources().getString(R.string.parking_occupancy_0free));
                txtViewParkingInformationsOccupancy.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_occupancy_0free));
            }
        } else {
            txtViewParkingInformationsOccupancy.setText(getResources().getString(R.string.parking_occupancy_noData));
            txtViewParkingInformationsOccupancy.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_occupancy_noData));
        }
        txtViewParkingInformationsPrice.setText("2 €/h");
        String travelTimeToString = String.valueOf(parkingData.getTravelTime()) + " min";
        txtViewTravelTime.setText(travelTimeToString);

        if (parkingData.getAddress() != null) {
            txtViewParkingAddress.setVisibility(View.VISIBLE);
            String parkingAddress = parkingData.getAddress().getAddressName() + ", " + parkingData.getAddress().getZip() + " " + parkingData.getAddress().getCity();
            txtViewParkingAddress.setText(parkingAddress);
        }
        if (parkingData.getPhone() != null) {
            txtViewParkingPhone.setVisibility(View.VISIBLE);
            String parkingPhone = parkingData.getPhone();
            txtViewParkingPhone.setText(parkingPhone);
            setOnPhoneClickListener(parkingPhone);
        }
        if (parkingData.getWebSite() != null) {
            txtViewParkingWebsite.setVisibility(View.VISIBLE);
            String parkingWebsite = parkingData.getWebSite();
            txtViewParkingWebsite.setText(parkingWebsite);
        }
    }

    private void setOnPhoneClickListener(final String phone_number) {
        txtViewParkingPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone_number));
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone_number));
                if (ActivityCompat.checkSelfPermission(ParkingDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(intent);
            }
        });
    }

    private void navigateUserToParking(Parking parking) {
        String targetLocation = parking.getLocation().getLat() + "," + parking.getLocation().getLng();

        String url = "http://maps.google.com/maps?saddr=" + myLocation + "&daddr=" + targetLocation;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }

    /*private class EndCallListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            String LOG_TAG = "EndCallListener";
            if(TelephonyManager.CALL_STATE_RINGING == state) {
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }
            if(TelephonyManager.CALL_STATE_OFFHOOK == state) {
                //wait for phone to go offhook (probably set a boolean flag) so you know your app initiated the call.
                Log.i(LOG_TAG, "OFFHOOK");
            }
            if(TelephonyManager.CALL_STATE_IDLE == state) {
                //when this state occurs, and your flag is set, restart your app
                Log.i(LOG_TAG, "IDLE");
            }
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab)
    public void onFloatingButtonClick(View view) {
        navigateUserToParking(parkingData);
    }
}
