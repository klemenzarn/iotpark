package si.versor.parking.layout.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import si.versor.parking.R;
import si.versor.parking.api.models.Parking;
import si.versor.parking.layout.adapters.DividerItemDecoration;
import si.versor.parking.layout.adapters.ParkingInforationsAdapter;

/**
 * Created by Matej on 3. 06. 2017.
 */

public class ParkingsInfoFragment extends Fragment {
    private List<Parking> parkingsList = null;
    private RecyclerView recyclerView;
    private ParkingInforationsAdapter mAdapter;

    private static final String PARKING_KEY = "parkings_key";
    private static final String MY_LOCATION_KEY = "myLocation_key";
    private ArrayList<Parking> mParkings;

    public static ParkingsInfoFragment newInstance(ArrayList<Parking> parkings, String myLocation) {
        ParkingsInfoFragment fragment = new ParkingsInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(PARKING_KEY, parkings);
        bundle.putString(MY_LOCATION_KEY, myLocation);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.parkings_info_layout, container, false);

        parkingsList = new ArrayList<>();

        mParkings = getArguments().getParcelableArrayList(PARKING_KEY);
        parkingsList.addAll(mParkings);

        recyclerView = (RecyclerView) view.findViewById(R.id.parkings_info_recycler_view);

        mAdapter = new ParkingInforationsAdapter(parkingsList, this.getActivity(), getArguments().getString(MY_LOCATION_KEY));
        RecyclerView.LayoutManager iLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(iLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        return view;
    }
}
