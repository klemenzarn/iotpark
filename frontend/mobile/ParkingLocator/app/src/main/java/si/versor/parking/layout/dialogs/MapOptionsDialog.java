package si.versor.parking.layout.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.versor.parking.R;

/**
 * Created by Matej on 24. 05. 2017.
 */

public class MapOptionsDialog extends Dialog {

    private Activity activity;
    private OnMapOptionsListener listener;

    @BindView(R.id.rGroupMap) RadioGroup rGroup;
    @BindView(R.id.rBtnShowNames) RadioButton rBtnShowNames;
    @BindView(R.id.rBtnShowOccupancy) RadioButton rBtnShowOccupancy;
    @BindView(R.id.rBtnShowPrice) RadioButton rBtnShowPrice;
    @BindView(R.id.switchIndoorParkings) Switch switchIndoorParkings;
    @BindView(R.id.switchOutdoorParkings) Switch switchOutdoorParkings;
    @BindView(R.id.switchFreeParkings) Switch switchFreeParkings;
    @BindView(R.id.switchPayableParkings) Switch switchPayableParkings;

    public MapOptionsDialog(Activity activity, OnMapOptionsListener listener) {
        super(activity);
        this.activity = activity;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dl_map_options);

        ButterKnife.bind(this);

        setInitalRBtnValue();
    }

    private void setInitalRBtnValue() {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);

        int rBtnValue = sharedPref.getInt(activity.getResources().getString(R.string.map_options_radio_button_value), R.id.rBtnShowOccupancy);

        switch (rBtnValue) {
            case R.id.rBtnShowNames:
                rBtnShowNames.setChecked(true);
                break;
            case R.id.rBtnShowOccupancy:
                rBtnShowOccupancy.setChecked(true);
                break;
            case R.id.rBtnShowPrice:
                rBtnShowPrice.setChecked(true);
                break;
            default:
                rBtnShowOccupancy.setChecked(true);
                break;
        }
    }
    
    @OnClick(R.id.btnDiscard)
    public void onDiscard(){
        dismiss();
    }

    @OnClick(R.id.btnDone)
    public void onOptionsSubmit(){
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        switch (rGroup.getCheckedRadioButtonId()) {
            case R.id.rBtnShowNames:
                editor.putInt(activity.getResources().getString(R.string.map_options_radio_button_value), R.id.rBtnShowNames);
                break;
            case R.id.rBtnShowOccupancy:
                editor.putInt(activity.getResources().getString(R.string.map_options_radio_button_value), R.id.rBtnShowOccupancy);
                break;
            case R.id.rBtnShowPrice:
                editor.putInt(activity.getResources().getString(R.string.map_options_radio_button_value), R.id.rBtnShowPrice);
                break;
            default:
                break;
        }

        /*if (switchIndoorParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing indoor parkings", Toast.LENGTH_SHORT).show();
        }
        if (switchOutdoorParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing outdoor parkings", Toast.LENGTH_SHORT).show();
        }
        if (switchFreeParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing free parkings", Toast.LENGTH_SHORT).show();
        }
        if (switchPayableParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing payable parkings", Toast.LENGTH_SHORT).show();
        }*/

        editor.apply();
        dismiss();

        if(listener != null) listener.onOptionsSubmit();
    }

    public interface OnMapOptionsListener{
        void onOptionsSubmit();
    }

}
