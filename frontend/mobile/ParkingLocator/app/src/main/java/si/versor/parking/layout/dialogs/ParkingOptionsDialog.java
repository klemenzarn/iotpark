package si.versor.parking.layout.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.versor.parking.R;

/**
 * Created by Matej on 24. 05. 2017.
 */

public class ParkingOptionsDialog extends Dialog {

    private Activity activty;
    private OnParkingOptionsListener listener;

    @BindView(R.id.rGroupParking) RadioGroup rGroup;
    @BindView(R.id.rBtnSortByDistance) RadioButton rBtnSortByDistance;
    @BindView(R.id.rBtnSortByOpening) RadioButton rBtnSortByOpening;
    @BindView(R.id.rBtnSortByPrice) RadioButton rBtnSortByPrice;
    @BindView(R.id.cBoxIndoorParkings) CheckBox cBoxIndoorParkings;
    @BindView(R.id.cBoxOutdoorParkings) CheckBox cBoxOutdoorParkings;
    @BindView(R.id.cBoxFreeParkings) CheckBox cBoxFreeParkings;
    @BindView(R.id.cBoxPayableParkings) CheckBox cBoxPayableParkings;

    public ParkingOptionsDialog(Activity activty, OnParkingOptionsListener listener) {
        super(activty);
        this.activty = activty;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dl_parking_options);

        ButterKnife.bind(this);
    }
    
    @OnClick(R.id.btnDiscard)
    public void onDiscard(){
        dismiss();
    }

    @OnClick(R.id.btnApply)
    public void onOptionsSubmit(){
        switch (rGroup.getCheckedRadioButtonId()) {
            case R.id.rBtnSortByDistance:
                Toast.makeText(this.activty, "Parkings sorted by distance", Toast.LENGTH_SHORT).show();
                break;
            case R.id.rBtnSortByOpening:
                Toast.makeText(this.activty, "Parkings sorted by opening", Toast.LENGTH_SHORT).show();
                break;
            case R.id.rBtnSortByPrice:
                Toast.makeText(this.activty, "Parkings sorted by price", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

        if (cBoxIndoorParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing indoor parkings", Toast.LENGTH_SHORT).show();
        }
        if (cBoxOutdoorParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing outdoor parkings", Toast.LENGTH_SHORT).show();
        }
        if (cBoxFreeParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing free parkings", Toast.LENGTH_SHORT).show();
        }
        if (cBoxPayableParkings.isChecked())
        {
            Toast.makeText(this.activty, "Showing payable parkings", Toast.LENGTH_SHORT).show();
        }

        dismiss();

        if(listener != null) listener.onOptionsSubmit();
    }

    public interface OnParkingOptionsListener {
        void onOptionsSubmit();
    }

}
