
package si.versor.parking.api.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Parking implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("hasSlotData")
    @Expose
    private Boolean hasSlotData;
    @SerializedName("fee")
    @Expose
    private Integer fee;
    @SerializedName("published")
    @Expose
    private Boolean published;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("webSite")
    @Expose
    private String webSite;
    @SerializedName("parking_occupancy")
    @Expose
    private Occupancy occupancy;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("working_type")
    @Expose
    private WorkingType workingType;
    @SerializedName("working_hours")
    @Expose
    private List<WorkingHour> workingHours = null;
    @SerializedName("fee_type")
    @Expose
    private FeeType feeType;
    @SerializedName("parking_fee_by_days")
    @Expose
    private List<Object> parkingFeeByDays = null;
    @SerializedName("managers")
    @Expose
    private List<Manager> managers = null;
    @SerializedName("providers")
    @Expose
    private List<Provider> providers = null;
    @SerializedName("parking_type")
    @Expose
    private ParkingType parkingType;
    @SerializedName("parking_category")
    @Expose
    private ParkingCategory parkingCategory;
    @SerializedName("areas")
    @Expose
    private List<Area> areas = null;
    private int travelTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasSlotData() {
        return hasSlotData;
    }

    public void setHasSlotData(Boolean hasSlotData) {
        this.hasSlotData = hasSlotData;
    }

    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public Occupancy getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(Occupancy occupancy) {
        this.occupancy = occupancy;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public WorkingType getWorkingType() {
        return workingType;
    }

    public void setWorkingType(WorkingType workingType) {
        this.workingType = workingType;
    }

    public List<WorkingHour> getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(List<WorkingHour> workingHours) {
        this.workingHours = workingHours;
    }

    public FeeType getFeeType() {
        return feeType;
    }

    public void setFeeType(FeeType feeType) {
        this.feeType = feeType;
    }

    public List<Object> getParkingFeeByDays() {
        return parkingFeeByDays;
    }

    public void setParkingFeeByDays(List<Object> parkingFeeByDays) {
        this.parkingFeeByDays = parkingFeeByDays;
    }

    public List<Manager> getManagers() {
        return managers;
    }

    public void setManagers(List<Manager> managers) {
        this.managers = managers;
    }

    public List<Provider> getProviders() {
        return providers;
    }

    public void setProviders(List<Provider> providers) {
        this.providers = providers;
    }

    public ParkingType getParkingType() {
        return parkingType;
    }

    public void setParkingType(ParkingType parkingType) {
        this.parkingType = parkingType;
    }

    public ParkingCategory getParkingCategory() {
        return parkingCategory;
    }

    public void setParkingCategory(ParkingCategory parkingCategory) {
        this.parkingCategory = parkingCategory;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public double getOccupancyPercent() {
        return occupancy.getOccupancyPercent();
    }

    public int getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(int travelTime) {
        this.travelTime = travelTime;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // Parcelling part
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeByte((byte) (hasSlotData ? 1 : 0));
        out.writeInt(fee);
        out.writeByte((byte) (published ? 1 : 0));
        out.writeString(phone);
        out.writeString(webSite);
        out.writeSerializable(occupancy);
        out.writeSerializable(address);
        out.writeSerializable(location);
        out.writeSerializable(workingType);
        out.writeList(workingHours);
        out.writeSerializable(feeType);
        out.writeList(parkingFeeByDays);
        out.writeList(managers);
        out.writeList(providers);
        out.writeSerializable(parkingType);
        out.writeSerializable(parkingCategory);
        out.writeList(areas);
        out.writeInt(travelTime);
        //out.writeDouble(location.getLat());
        //out.writeDouble(location.getLng());
        //out.writeDouble(price);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Parking createFromParcel(Parcel in) {
            return new Parking(in);
        }

        @Override
        public Parking[] newArray(int size) {
            return new Parking[size];
        }
    };

    private Parking(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.hasSlotData = in.readByte() != 0;
        this.fee = in.readInt();
        this.published = in.readByte() != 0;
        this.phone = in.readString();
        this.webSite = in.readString();
        this.occupancy = (Occupancy) in.readSerializable();
        this.address = (Address) in.readSerializable();
        this.location = (Location) in.readSerializable();
        this.workingType = (WorkingType) in.readSerializable();
        this.workingHours = new ArrayList<WorkingHour>();
        in.readList(this.workingHours, null);
        this.feeType = (FeeType) in.readSerializable();
        this.parkingFeeByDays = new ArrayList<Object>();
        in.readList(this.parkingFeeByDays, null);
        this.managers = new ArrayList<Manager>();
        in.readList(this.managers, null);
        this.providers = new ArrayList<Provider>();
        in.readList(this.providers, null);
        this.parkingType = (ParkingType) in.readSerializable();
        this.parkingCategory = (ParkingCategory) in.readSerializable();
        this.areas = new ArrayList<Area>();
        in.readList(this.areas, null);
        this.travelTime = in.readInt();
        //this.location = new Location();
        //this.location.setLat(in.readDouble());
        //this.location.setLng(in.readDouble());
    }

}
