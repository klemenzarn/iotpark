package si.versor.parking.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by klemen on 22. 03. 2017.
 */
public class Occupancy implements Serializable {

    @SerializedName("occupancy")
    @Expose
    private int occupiedSlots;

    @SerializedName("capacity")
    @Expose
    private int allSlots;

    public int getAllSlots() {
        return allSlots;
    }

    public void setAllSlots(int allSlots) {
        this.allSlots = allSlots;
    }

    public int getOccupiedSlots() {
        return occupiedSlots;
    }

    public void setOccupiedSlots(int occupiedSlots) {
        this.occupiedSlots = occupiedSlots;
    }

    public double getOccupancyPercent() {
        return (double) this.getOccupiedSlots() / this.getAllSlots() * 100.0;
    }

    public int getFreeSlots(){
        return this.getAllSlots() - this.getOccupiedSlots();
    }

    @Override
    public String toString() {
        return "Occupancy{" +
                "occupiedSlots=" + occupiedSlots +
                ", allSlots=" + allSlots +
                '}';
    }
}
