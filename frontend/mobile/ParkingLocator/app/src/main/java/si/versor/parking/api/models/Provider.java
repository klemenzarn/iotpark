
package si.versor.parking.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class Provider implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("parking_provider")
    @Expose
    private ParkingProvider parkingProvider;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParkingProvider getParkingProvider() {
        return parkingProvider;
    }

    public void setParkingProvider(ParkingProvider parkingProvider) {
        this.parkingProvider = parkingProvider;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
