package si.versor.parking.api.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by klemen on 22. 03. 2017.
 */
public class Slot implements Serializable {
    private int ID;
    private String name;
    private int occupied;

    @SerializedName("area_id")
    private int areaId;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccupied() {
        return occupied;
    }

    public void setOccupied(int occupied) {
        this.occupied = occupied;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    @Override
    public String toString() {
        return "Slot{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", occupied=" + occupied +
                ", areaId=" + areaId +
                '}';
    }
}
