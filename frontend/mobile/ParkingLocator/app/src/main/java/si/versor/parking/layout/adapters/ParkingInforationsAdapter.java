package si.versor.parking.layout.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import si.versor.parking.R;
import si.versor.parking.api.models.Occupancy;
import si.versor.parking.api.models.Parking;
import si.versor.parking.layout.ParkingDetailsActivity;

/**
 * Created by Matej on 2. 06. 2017.
 */

public class ParkingInforationsAdapter extends RecyclerView.Adapter<ParkingInforationsAdapter.ParkingInformationsViewHolder> {

    private List<Parking> parkingsList;
    private Activity activity;
    private String myLocation;

    public class ParkingInformationsViewHolder extends RecyclerView.ViewHolder {
        public TextView title, occupancy, price, travelTime;
        public LinearLayout linearLayout;

        public ParkingInformationsViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.txtViewParkingInformationsTitle);
            occupancy = (TextView) view.findViewById(R.id.txtViewParkingInformationsOccupancy);
            price = (TextView) view.findViewById(R.id.txtViewParkingInformationsPrice);
            travelTime = (TextView) view.findViewById(R.id.txtViewTravelTime);
            linearLayout = (LinearLayout) view.findViewById(R.id.parkingInfoLayout);
        }
    }

    public ParkingInforationsAdapter(List<Parking> parkingsList, Activity ac, String myLocation) {
        this.parkingsList = parkingsList;
        this.activity = ac;
        this.myLocation = myLocation;
    }

    @Override
    public ParkingInformationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.parking_info, parent, false);

        return new ParkingInformationsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ParkingInforationsAdapter.ParkingInformationsViewHolder holder, int position) {
        final Parking parking = parkingsList.get(position);
        Occupancy parkingOccupancy = parking.getOccupancy();

        holder.title.setText(parking.getName());
        if (parkingOccupancy != null) {
            if (parkingOccupancy.getOccupancyPercent() == 100) {
                holder.occupancy.setText(activity.getResources().getString(R.string.parking_occupancy_0free));
                holder.occupancy.setTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.color_occupancy_0free));
            }
            else {
                holder.occupancy.setText(String.format(Locale.getDefault(), "%1$d FREE", parkingOccupancy.getFreeSlots()));
                holder.occupancy.setTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.color_occupancy_free));
            }
        }
        else {
            holder.occupancy.setText(activity.getResources().getString(R.string.parking_occupancy_noData));
            holder.occupancy.setTextColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.color_occupancy_noData));
        }
        //TODO holder.price.setText(String.format(Locale.getDefault(), "%1$s €/h", String.valueOf(parking.getPrice())));
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent parkingDetailsActivity = new Intent(activity, ParkingDetailsActivity.class);
                parkingDetailsActivity.putExtra("parking", parking);
                parkingDetailsActivity.putExtra("myLocation", myLocation);
                activity.startActivity(parkingDetailsActivity);
            }
        });
        String travelTimeToString = String.valueOf(parking.getTravelTime()) + " min";
        holder.travelTime.setText(travelTimeToString);
    }

    @Override
    public int getItemCount() {
        return parkingsList.size();
    }
}
