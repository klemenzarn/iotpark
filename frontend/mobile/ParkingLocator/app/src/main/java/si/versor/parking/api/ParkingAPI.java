package si.versor.parking.api;

import android.util.Log;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import si.versor.parking.core.Config;

/**
 * Created by klemen on 28. 03. 2017.
 */
public class ParkingAPI implements IParkingAPI {

    public static String URL = "https://iotpark.now.sh/";//"http://192.168.178.42:4083/";//http://docs.versor.si:3508/";
    public static String SECURE_URL = "https://iotpark.now.sh/";//https://192.168.178.42:4080/";//"https://docs.versor.si:3444/";

    @Override
    public ParkingService initRestClient() {

        OkHttpClient httpClient = new OkHttpClient();
        String apiUrl = URL;

        if (Config.SECURE_CONNECTION) {
            apiUrl = SECURE_URL;
            try {
                httpClient = UnsafeOkHttpClient.getClient();
            } catch (RuntimeException ex) {
                //fallback to http client
                apiUrl = URL;
                httpClient = new OkHttpClient();
                ex.printStackTrace();
                //TODO log and notify error at creating UnsafeOkHttpClient for HTTPS requests
            }
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Log.d(Config.LOG_TAG, "Creating rest api with url: " + apiUrl + ", connection is secure: " + Config.SECURE_CONNECTION);
        return retrofit.create(ParkingService.class);
    }


}
