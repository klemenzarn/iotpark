package si.versor.parking.api.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


/**
 * Created by klemen on 22. 03. 2017.
 */
public class Area implements Serializable {
    @SerializedName("id")
    private int ID;

    @SerializedName("shortName")
    private String shortName;

    @SerializedName("fullName")
    private String fullName;

    @SerializedName("parkingId")
    private int parkingId;

    @SerializedName("area_occupancy")
    private Occupancy occupancy;

    private List<Slot> slots;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }

    public Occupancy getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(Occupancy occupancy) {
        this.occupancy = occupancy;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    @Override
    public String toString() {
        return "Area{" +
                "ID=" + ID +
                ", shortName='" + shortName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", parkingId=" + parkingId +
                ", occupancy=" + occupancy +
                ", slots=" + slots +
                '}';
    }
}
