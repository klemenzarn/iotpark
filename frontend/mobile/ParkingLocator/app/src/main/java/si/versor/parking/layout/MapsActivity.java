package si.versor.parking.layout;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.versor.parking.R;
import si.versor.parking.api.IParkingAPI;
import si.versor.parking.api.ParkingAPI;
import si.versor.parking.api.ParkingService;
import si.versor.parking.api.models.Occupancy;
import si.versor.parking.api.models.Parking;
import si.versor.parking.layout.dialogs.Dialogs;
import si.versor.parking.layout.dialogs.MapOptionsDialog;
import si.versor.parking.layout.dialogs.ParkingOptionsDialog;
import si.versor.parking.layout.fragments.ParkingsInfoFragment;

public class MapsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab_bottom)
    FloatingActionButton floatingBtnBottom;
    @BindView(R.id.fab_upper)
    FloatingActionButton floatingBtnUpper;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.loading_circle)
    ProgressBar loadingCircle;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.infoLayout)
    FrameLayout infoLayout;

    private GoogleMap mapView;
    private ParkingService parkingService;
    private Dialogs dialogs;
    private GoogleApiClient mGoogleApiClient;

    private ProgressDialog progressDialog;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private LocationRequest mLocationRequest;
    private final static int REQUEST_LOCATION = 199;

    private Location mCurrLocation;
    private Marker mCurrLocationMarker;

    // store google maps location id with parking
    private HashMap<String, Parking> parkingIds = new HashMap<>();

    private List<Marker> markers = null;
    private Marker currParkingMarker = null;

    private SharedPreferences sharedPref;

    private final static int SLIDING_LAYOUT_VISIBLE = 1;
    private final static int SLIDING_LAYOUT_GONE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // set theme
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Log.d("Test", "Initial test");

        dialogs = new Dialogs(MapsActivity.this);

        IParkingAPI api = new ParkingAPI();
        parkingService = api.initRestClient();

        //init layout
        initComponents();
    }

    private void initComponents() {
        ButterKnife.bind(this);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // show toolbar
        appBarLayout.bringToFront();

        slidingLayout.setOnTouchListener(onHideListener());

        sharedPref = MapsActivity.this.getPreferences(Context.MODE_PRIVATE);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    /**
     * Hide sliding layout if it is displayed
     * otherwise pause the app and save state
     */
    @Override
    public void onBackPressed() {
        if (slidingLayout.getPanelHeight() > 0) {
            hideSlidingLayoutAndShowFloatingButtons();
        } else {
            Intent setIntent = new Intent(Intent.ACTION_MAIN);
            setIntent.addCategory(Intent.CATEGORY_HOME);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(setIntent);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapView = googleMap;

        mapView.getUiSettings().setMapToolbarEnabled(false);
        // mapView.getUiSettings().setZoomControlsEnabled(false);

        mapView.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (parkingIds.get(marker.getId()) != null) {
                    IconGenerator iconFactory = new IconGenerator(getApplicationContext());

                    if (currParkingMarker != null)
                        modifyCurrentMarker(iconFactory, SLIDING_LAYOUT_GONE);

                    currParkingMarker = marker;
                    iconFactory.setColor(Color.BLACK);

                    modifyCurrentMarker(iconFactory, SLIDING_LAYOUT_VISIBLE);

                    floatingBtnUpper.setVisibility(View.VISIBLE);
                    floatingBtnBottom.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.floating_btn_background_color_black)));
                    floatingBtnBottom.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_directions_white_48dp));
                    setInfoLayoutFragment(parkingIds, currParkingMarker.getId());  // set parking data
                }

                return false;
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mapView.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mapView.setMyLocationEnabled(true);
        }
    }

    private void modifyCurrentMarker(IconGenerator iconFactory, int sliding_layout_visibility) {
        int rBtnValue = sharedPref.getInt(getString(R.string.map_options_radio_button_value), R.id.rBtnShowOccupancy);

        Parking parkingData = parkingIds.get(currParkingMarker.getId());
        String iconText;

        switch (rBtnValue) {
            case R.id.rBtnShowNames:
                parkingData = parkingIds.get(currParkingMarker.getId());
                if (sliding_layout_visibility == SLIDING_LAYOUT_VISIBLE)
                    iconFactory.setTextAppearance(R.style.map_iconText_white);
                else
                    iconFactory.setTextAppearance(R.style.map_iconText_black);
                iconText = parkingData.getName();
                currParkingMarker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(iconText)));
                break;
            case R.id.rBtnShowOccupancy:
                Occupancy parkingOccupancy = parkingData.getOccupancy();
                if (parkingOccupancy != null) {
                    if (parkingOccupancy.getFreeSlots() > 0) {
                        iconFactory.setTextAppearance(R.style.map_iconText_free);
                        iconText = parkingOccupancy.getFreeSlots() + " FREE";
                    } else {
                        iconFactory.setTextAppearance(R.style.map_iconText_occupied);
                        iconText = getResources().getString(R.string.parking_occupancy_0free);
                    }
                } else {
                    if (sliding_layout_visibility == SLIDING_LAYOUT_VISIBLE)
                        iconFactory.setTextAppearance(R.style.map_iconText_white);
                    else
                        iconFactory.setTextAppearance(R.style.map_iconText_black);
                    iconText = getResources().getString(R.string.parking_occupancy_noData);
                }

                currParkingMarker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(iconText)));
                break;
            case R.id.rBtnShowPrice:
                parkingData = parkingIds.get(currParkingMarker.getId());
                if (sliding_layout_visibility == SLIDING_LAYOUT_VISIBLE)
                    iconFactory.setTextAppearance(R.style.map_iconText_white);
                else
                    iconFactory.setTextAppearance(R.style.map_iconText_black);
                iconText = "2 €";
                currParkingMarker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(iconText)));
                break;
            default:
                break;
        }
    }

    private void showNameOnMarkers(IconGenerator iconFactory) {
        iconFactory.setTextAppearance(R.style.map_iconText_black);

        for (Marker m : markers) {
            Parking parkingData = parkingIds.get(m.getId());
            String iconText = parkingData.getName();
            m.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(iconText)));
        }
    }

    private void showOccupancyOnMarkers(IconGenerator iconFactory) {
        for (Marker m : markers) {
            Occupancy parkingOccupancy = parkingIds.get(m.getId()).getOccupancy();
            String iconText;
            if (parkingOccupancy != null) {
                if (parkingOccupancy.getFreeSlots() > 0) {
                    iconFactory.setTextAppearance(R.style.map_iconText_free);
                    iconText = parkingOccupancy.getFreeSlots() + " FREE";
                } else {
                    iconFactory.setTextAppearance(R.style.map_iconText_occupied);
                    iconText = getResources().getString(R.string.parking_occupancy_0free);
                }
            } else {
                iconFactory.setTextAppearance(R.style.map_iconText_black);
                iconText = getResources().getString(R.string.parking_occupancy_noData);
            }

            m.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(iconText)));
        }
    }

    private void showPriceOnMarkers(IconGenerator iconFactory) {
        iconFactory.setTextAppearance(R.style.map_iconText_black);

        for (Marker m : markers) {
            Parking parkingData = parkingIds.get(m.getId());
            String iconText = "2 €";
            m.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(iconText)));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mapView.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                // return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mGoogleApiClient != null && mLocationRequest != null) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }

            checkIfLocationIsEnabled();
        }
    }

    @Override
    public void onMapLoaded() {
        getParkings();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        boolean hideDrawer = true;

        if (id == R.id.nav_indoor_parkings) {
            Toast.makeText(MapsActivity.this, "Inodoor parkings", Toast.LENGTH_LONG).show();
        }
        //TODO other nav drawer menu buttons and actions

        if (hideDrawer) {
            drawer.closeDrawer(GravityCompat.START);
        }

        return true;

    }

    @OnClick(R.id.fab_bottom)
    public void onFloatingButtonBottomClick(View view) {
        if (floatingBtnUpper.getVisibility() == View.VISIBLE) {
            if (slidingLayout.getPanelHeight() > 0) {
                navigateUserToParking(parkingIds.get(currParkingMarker.getId()));
            } else {
                fBtnBottomListAction();
            }
        } else {
            if (slidingLayout.getPanelHeight() > 0) {
                fBtnBottomParkingsFilter();
            }
            else {
                fBtnBottomPAction();
            }
        }
    }

    @OnClick(R.id.fab_upper)
    public void onFloatingButtomUpperClick(View view) {
        dialogs.showMapOptionsDialog(new MapOptionsDialog.OnMapOptionsListener() {
            @Override
            public void onOptionsSubmit() {
                IconGenerator iconFactory = new IconGenerator(getApplicationContext());

                int rBtnValue = sharedPref.getInt(getString(R.string.map_options_radio_button_value), R.id.rBtnShowOccupancy);

                switch (rBtnValue) {
                    case R.id.rBtnShowNames:
                        showNameOnMarkers(iconFactory);
                        break;
                    case R.id.rBtnShowOccupancy:
                        showOccupancyOnMarkers(iconFactory);
                        break;
                    case R.id.rBtnShowPrice:
                        showPriceOnMarkers(iconFactory);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        checkIfLocationIsEnabled();
    }

    private void checkIfLocationIsEnabled() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MapsActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode)
        {
            case REQUEST_LOCATION:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                    {
                        // All required changes were successfully made
                        // Toast.makeText(MapsActivity.this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        // The user was asked to change settings, but chose not to
                        // Toast.makeText(MapsActivity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        /* Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent); */
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("Test", "on loc change " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());
        mapView.setTrafficEnabled(true);

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        // Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_car_map));
        markerOptions.anchor(0.5f, 0.5f);
        mCurrLocationMarker = mapView.addMarker(markerOptions);
        mCurrLocation = location;

        // move camera to current location only when parkings not rendered yet
        if (parkingIds.isEmpty()) {
            mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
        }
    }

    /**
     * Hide sliding layout when touch sliding layout
     */
    private View.OnTouchListener onHideListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP)
                    hideSlidingLayoutAndShowFloatingButtons();

                return false;
            }
        };
    }

    private void hideSlidingLayoutAndShowFloatingButtons() {
        IconGenerator iconFactory = new IconGenerator(this);

        if (currParkingMarker != null)
            modifyCurrentMarker(iconFactory, SLIDING_LAYOUT_GONE);

        slidingLayout.setPanelHeight(pxToDp(0));    // set height of layout
        floatingBtnUpper.setVisibility(View.VISIBLE);
        floatingBtnUpper.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_floating_btn_more));
        floatingBtnBottom.setVisibility(View.VISIBLE);
        floatingBtnBottom.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getApplicationContext(), R.color.floating_btn_background_color_white)));
        floatingBtnBottom.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_floating_btn_list));
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void getParkings() {
        Log.d("Test","gre po podatke");
        Call<List<Parking>> parkingsRequest = parkingService.getParkings();
        parkingsRequest.enqueue(new Callback<List<Parking>>() {
            @Override
            public void onResponse(Call<List<Parking>> call, Response<List<Parking>> response) {
                Log.d("Test","dobi neke podatke");
                List<Parking> parkings = response.body();
                if (parkings != null && response.code() == 200) {
                    renderParkings(parkings);
                } else {
                    Log.d("Test","parking != null && code == 200");
                    alertApiFail(new Exception("Error getting graves, graves object is null or status code is not 200 (current code is " + response.code() + ")"));
                }
            }

            @Override
            public void onFailure(Call<List<Parking>> call, Throwable t) {
                alertApiFail(t);
                Log.e("Test","ne dobi nič", t);
            }
        });
    }


    private Bitmap getIcon(Parking parkingData) {
        Occupancy parkingOccupancy = parkingData.getOccupancy();

        IconGenerator iconFactory = new IconGenerator(this);
        String iconText;

        int rBtnValue = sharedPref.getInt(getString(R.string.map_options_radio_button_value), R.id.rBtnShowOccupancy);

        switch (rBtnValue) {
            case R.id.rBtnShowNames:
                iconFactory.setTextAppearance(R.style.map_iconText_black);
                iconText = parkingData.getName();
                break;
            case R.id.rBtnShowOccupancy:
                if (parkingOccupancy != null) {
                    if (parkingOccupancy.getFreeSlots() > 0) {
                        iconFactory.setTextAppearance(R.style.map_iconText_free);
                        iconText = parkingOccupancy.getFreeSlots() + " FREE";
                    } else {
                        iconFactory.setTextAppearance(R.style.map_iconText_occupied);
                        iconText = getResources().getString(R.string.parking_occupancy_0free);
                    }
                } else {
                    iconFactory.setTextAppearance(R.style.map_iconText_black);
                    iconText = getResources().getString(R.string.parking_occupancy_noData);
                }
                break;
            case R.id.rBtnShowPrice:
                iconFactory.setTextAppearance(R.style.map_iconText_black);
                iconText = "2 €";
                break;
            default:
                if (parkingOccupancy != null) {
                    if (parkingOccupancy.getFreeSlots() > 0) {
                        iconFactory.setTextAppearance(R.style.map_iconText_free);
                        iconText = parkingOccupancy.getFreeSlots() + " FREE";
                    } else {
                        iconFactory.setTextAppearance(R.style.map_iconText_occupied);
                        iconText = getResources().getString(R.string.parking_occupancy_0free);
                    }
                } else {
                    iconFactory.setTextAppearance(R.style.map_iconText_black);
                    iconText = getResources().getString(R.string.parking_occupancy_noData);
                }
                break;
        }

        return iconFactory.makeIcon(iconText);
    }

    private void renderParkings(final List<Parking> parkings) {
        markers = new ArrayList<>();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        boolean hasData = parkings != null && parkings.size() > 0;
        if (hasData) {
            for (Parking parkingData : parkings) {

                si.versor.parking.api.models.Location parkingLocation = parkingData.getLocation();
                LatLng tempMarker = new LatLng(parkingLocation.getLat(),parkingLocation.getLng());
                Marker marker = mapView.addMarker(new MarkerOptions()
                        .position(tempMarker)
                        .icon(BitmapDescriptorFactory.fromBitmap(getIcon(parkingData)))
                );

                builder.include(tempMarker);
                parkingIds.put(marker.getId(), parkingData);
                markers.add(marker);
            }

            LatLngBounds bounds = builder.build();

            int padding = getResources().getInteger(R.integer.map_padding);
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

            mapView.animateCamera(cu);

            if(progressDialog != null)
            progressDialog.dismiss();
        }

    }

    private int setPanelHeightInDp(int numOfParkings) {
        int px = numOfParkings * 80;   // 80 dp is layout_height of one "parking_info.xml"

        return pxToDp(px);
    }

    private void alertApiFail(Throwable t) {
        dialogs.showConnectionAlert();

        floatingBtnUpper.setVisibility(View.GONE);
        floatingBtnBottom.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_outdoor_parkings));
        if (progressDialog != null)
            progressDialog.dismiss();

        //TODO log error to our server
    }

    private int pxToDp(int px) {
        float scale = getResources().getDisplayMetrics().density;
        return (int) (px*scale + 0.5f);
    }

    private void setInfoLayoutFragment(HashMap<String, Parking> parkings, String parkingId) {
        ArrayList<Parking> parkingsArrayList = new ArrayList<>();
        String myLocation = mCurrLocation.getLatitude() + "," + mCurrLocation.getLongitude();

        if (parkingId != null) {
            parkingsArrayList.add(parkings.get(parkingId));
            slidingLayout.setPanelHeight(setPanelHeightInDp(1));    // set height of layout
        }
        else {
            parkingsArrayList = new ArrayList<>(parkings.values());
            slidingLayout.setPanelHeight(setPanelHeightInDp(parkings.size()));    // set height of layout
        }

        for (Parking p : parkingsArrayList) {
            p.setTravelTime(calculateTravelTime(p.getLocation(), mCurrLocation));
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ParkingsInfoFragment fragment = ParkingsInfoFragment.newInstance(parkingsArrayList, myLocation);
        ft.replace(R.id.infoLayout, fragment);
        ft.commit();
    }

    private void fBtnBottomPAction() {
        progressDialog = ProgressDialog.show(this, "Loading parkings", "Please wait...", true);

        mapView.setOnMapLoadedCallback(this);

        floatingBtnBottom.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_floating_btn_list));
        floatingBtnUpper.setVisibility(View.VISIBLE);
    }

    private void fBtnBottomListAction() {
        floatingBtnUpper.setVisibility(View.GONE);
        floatingBtnBottom.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_floating_btn_more));
        setInfoLayoutFragment(parkingIds, null);  // set parkings data
    }

    private void fBtnBottomParkingsFilter() {
        dialogs.showParkingOptionsDialog(new ParkingOptionsDialog.OnParkingOptionsListener() {
            @Override
            public void onOptionsSubmit() {
            }
        });
    }

    private void navigateUserToParking(Parking parking) {
        String myLocation = mCurrLocation.getLatitude() + "," + mCurrLocation.getLongitude();
        String targetLocation = parking.getLocation().getLat() + "," + parking.getLocation().getLng();

        String url = "http://maps.google.com/maps?saddr=" + myLocation + "&daddr=" + targetLocation;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }

    private int calculateTravelTime(si.versor.parking.api.models.Location location1, Location location2) {
        return (int) ((distance(location1, location2) / 50) * 60);
    }

    public static float distance(si.versor.parking.api.models.Location parkingLocation, Location location2) {
        Location location1 = new Location("");
        location1.setLatitude(parkingLocation.getLat());
        location1.setLongitude(parkingLocation.getLng());

        return (location1.distanceTo(location2) / 1000);
    }
}
