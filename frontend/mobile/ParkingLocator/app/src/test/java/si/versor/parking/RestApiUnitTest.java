package si.versor.parking;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import si.versor.parking.api.IParkingAPI;
import si.versor.parking.api.ParkingService;
import si.versor.parking.api.models.old.Area;
import si.versor.parking.api.models.old.Occupancy;
import si.versor.parking.api.models.old.Parking;
import si.versor.parking.api.models.Slot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class RestApiUnitTest {

    ParkingService parkingService;

    @Before
    public void initApiClient() {
        IParkingAPI api = new ParkingAPITest();
        parkingService = api.initRestClient();
    }

    @Test
    public void weGetTwoParkings() throws InterruptedException {
        final Object syncObject = new Object();

        Call<List<Parking>> parkingList = parkingService.getParkings();
        parkingList.enqueue(new Callback<List<Parking>>() {
            @Override
            public void onResponse(Call<List<Parking>> call, Response<List<Parking>> response) {
                if (response.code() == 200) {
                    List<Parking> parkings = response.body();

                    assertEquals(2, parkings.size());
                } else {
                    fail("Not successful");
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<List<Parking>> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }

    }

    @Test
    public void weGetParkingOverallData() throws InterruptedException {
        final Object syncObject = new Object();

        Call<Parking> parkingApiCall = parkingService.getParking(1);
        parkingApiCall.enqueue(new Callback<Parking>() {
            @Override
            public void onResponse(Call<Parking> call, Response<Parking> response) {
                if (response.body() != null && response.code() == 200) {
                    assertTrue(true);
                } else {
                    fail("Response error");
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<Parking> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }

    @Test
    public void weGetAreaData() throws InterruptedException {
        final Object syncObject = new Object();

        Call<Area> areaApiCall = parkingService.getArea(1);
        areaApiCall.enqueue(new Callback<Area>() {
            @Override
            public void onResponse(Call<Area> call, Response<Area> response) {

                if (response.body() != null && response.code() == 200) {
                    assertTrue(true);
                } else {
                    fail("Response error");
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<Area> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }

    }

    @Test
    public void weGetSlotData() throws InterruptedException {
        final Object syncObject = new Object();

        Call<Slot> slotApiCall = parkingService.getSlot(44);
        slotApiCall.enqueue(new Callback<Slot>() {
            @Override
            public void onResponse(Call<Slot> call, Response<Slot> response) {
                if (response.body() != null && response.code() == 200) {
                    assertTrue(true);
                } else {
                    fail("Response error");
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<Slot> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }

    @Test
    public void isSlotNotFound() throws Exception {
        final Object syncObject = new Object();

        Call<Slot> slotApiCall = parkingService.getSlot(1);
        slotApiCall.enqueue(new Callback<Slot>() {
            @Override
            public void onResponse(Call<Slot> call, Response<Slot> response) {
                if (response.body() != null && response.code() == 200) {
                    fail("There is a slot with testing ID");
                } else {
                    assertTrue(true);
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<Slot> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }

    @Test
    public void isAreaOccupancyCorrect() throws InterruptedException {
        final Object syncObject = new Object();

        Call<Area> areaApiCall = parkingService.getArea(3);
        areaApiCall.enqueue(new Callback<Area>() {
            @Override
            public void onResponse(Call<Area> call, Response<Area> response) {

                if (response.body() != null && response.code() == 200) {
                    Area area = response.body();
                    Occupancy areaOccupancy = area.getOccupancy();
                    assertTrue(areaOccupancy.getAllSlots() == 249 && areaOccupancy.getOccupiedSlots() == 112);
                } else {
                    fail("Response error");
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<Area> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }

    @Test
    public void isParkingOccupancyCorrect() throws InterruptedException {
        final Object syncObject = new Object();

        Call<Parking> parkingApiCall = parkingService.getParking(1);
        parkingApiCall.enqueue(new Callback<Parking>() {
            @Override
            public void onResponse(Call<Parking> call, Response<Parking> response) {
                if (response.body() != null && response.code() == 200) {
                    Parking parking = response.body();
                    assertTrue(parking.getAllSlots() == 1000 && parking.getFreeSlots() == 489);
                } else {
                    fail("Response error");
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<Parking> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }

    @Test
    public void weGetCorrectNumberOfAreas() throws InterruptedException {
        final Object syncObject = new Object();

        Call<Parking> parkingApiCall = parkingService.getParking(1);
        parkingApiCall.enqueue(new Callback<Parking>() {
            @Override
            public void onResponse(Call<Parking> call, Response<Parking> response) {
                if (response.body() != null && response.code() == 200) {
                    Parking parking = response.body();
                    assertTrue(parking.getAreas().size() == 4);
                } else {
                    fail("Response error");
                }
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

            @Override
            public void onFailure(Call<Parking> call, Throwable t) {
                fail(t.toString());
                synchronized (syncObject) {
                    syncObject.notify();
                }
            }
        });

        synchronized (syncObject) {
            syncObject.wait();
        }
    }
}