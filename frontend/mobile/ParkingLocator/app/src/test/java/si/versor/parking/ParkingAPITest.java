package si.versor.parking;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import si.versor.parking.api.IParkingAPI;
import si.versor.parking.api.ParkingService;
import si.versor.parking.api.UnsafeOkHttpClient;
import si.versor.parking.core.Config;

/**
 * Created by Klemen on 16. 04. 2017.
 */

public class ParkingAPITest implements IParkingAPI {

    public String URL = "http://docs.versor.si:3508/";

    @Override
    public ParkingService initRestClient() {
        OkHttpClient httpClient = new OkHttpClient();
        String apiUrl = URL;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ParkingService.class);
    }
}
