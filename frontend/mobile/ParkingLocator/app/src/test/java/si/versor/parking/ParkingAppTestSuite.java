package si.versor.parking;

import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Klemen on 16. 04. 2017.
 */

@RunWith(Suite.class)

@Suite.SuiteClasses({
        RestApiUnitTest.class
})
public class ParkingAppTestSuite {
}
